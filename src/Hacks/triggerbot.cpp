#include "triggerbot.h"
//#include "autowall.h"

bool triggerbot_enabled = true;
bool enemies = true;
bool allies = false; // allies
bool walls = false; // not working
bool smokeCheck = false;
bool flashCheck = false;
bool head = true;
bool chest = true;
bool stomach = true;
bool arms = true;
bool legs = true;
bool triggerbot_enabled_random_delay = true;
//	Default tickrate is 64
//	1000 / 64 = 15.625 ms
//	Delta between ticks is 15.625 ms
//	Default player speed with knife is 250 units/s (Info from CS 1.6)
//	250 / 64 = 3.90625 units/s
//	FOR AN IDEAL TRIGGER DISABLE INTERPOLATION

//int random_delay_lowbound = 16; // minimum
//int random_delay_highbound = 32; // maximum

int random_delay_lowbound = 10; // minimum
int random_delay_highbound = 30; // maximum

int random_delay_lastroll = 0;
ButtonCode_t triggerbot_keycode = ButtonCode_t::MOUSE_4;

void Triggerbot::CreateMove(CUserCmd *cmd)
{
	if (!triggerbot_enabled)
		return;

	if (!inputSystem->IsButtonDown(triggerbot_keycode))
		return;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	if (!localplayer || !localplayer->GetAlive())
		return;
	
	if (flashCheck && localplayer->GetFlashBangTime() - globalVars->curtime > 2.0f)
		return;

	long currentTime_ms = Util::GetEpochTime();
	static long timeStamp = currentTime_ms;
	long oldTimeStamp;


	static int localMin = random_delay_lowbound;
	static int localMax = random_delay_highbound;
	static int randomDelay = localMin + rand() % (localMax - localMin);

	if( localMin != random_delay_lowbound || localMax != random_delay_highbound ) // Done in case Low/high bounds change before the next triggerbot shot.
	{
		localMin = random_delay_lowbound;
		localMax = random_delay_highbound;
		randomDelay = localMin + rand() % (localMax - localMin);
	}


	Vector traceStart, traceEnd;
	trace_t tr;

	QAngle viewAngles;
	engine->GetViewAngles(viewAngles);
	QAngle viewAngles_rcs = viewAngles + *localplayer->GetAimPunchAngle() * 2.0f;

	Math::AngleVectors(viewAngles_rcs, traceEnd);

	traceStart = localplayer->GetEyePosition();
	traceEnd = traceStart + (traceEnd * 8192.0f);
	/*
	if (walls)
	{
		Autowall::FireBulletData data;
		if (Autowall::GetDamage(traceEnd, !allies, data) == 0.0f)
			return;

		tr = data.enter_trace;
	}
	*/
	//else
	//{
		Ray_t ray;
		ray.Init(traceStart, traceEnd);
		CTraceFilter traceFilter;
		traceFilter.pSkip = localplayer;
		trace->TraceRay(ray, 0x46004003, &traceFilter, &tr);
	//}

	oldTimeStamp = timeStamp;
	timeStamp = currentTime_ms;

	C_BasePlayer* player = (C_BasePlayer*) tr.m_pEntityHit;
	if (!player)
		return;

	if (player->GetClientClass()->m_ClassID != EClassIds::CCSPlayer)
		return;

	if (player == localplayer
		|| player->GetDormant()
		|| !player->GetAlive()
		|| player->GetImmune())
		return;

	if (player->GetTeam() != localplayer->GetTeam() && !enemies)
		return;

	if (player->GetTeam() == localplayer->GetTeam() && !allies)
		return;

	bool filter;

	switch (tr.hitgroup)
	{
		case HitGroups::HITGROUP_HEAD:
			filter = head;
			break;
		case HitGroups::HITGROUP_CHEST:
			filter = chest;
			break;
		case HitGroups::HITGROUP_STOMACH:
			filter = stomach;
			break;
		case HitGroups::HITGROUP_LEFTARM:
		case HitGroups::HITGROUP_RIGHTARM:
			filter = arms;
			break;
		case HitGroups::HITGROUP_LEFTLEG:
		case HitGroups::HITGROUP_RIGHTLEG:
			filter = legs;
			break;
		default:
			filter = false;
	}

	if (!filter)
		return;

	if (smokeCheck && LineGoesThroughSmoke(tr.startpos, tr.endpos, 1))
		return;

	C_BaseCombatWeapon* activeWeapon = (C_BaseCombatWeapon*) entityList->GetClientEntityFromHandle(localplayer->GetActiveWeapon());
	if (!activeWeapon || activeWeapon->GetAmmo() == 0)
		return;

	ItemDefinitionIndex itemDefinitionIndex = *activeWeapon->GetItemDefinitionIndex();
	if (itemDefinitionIndex == ItemDefinitionIndex::WEAPON_KNIFE || itemDefinitionIndex >= ItemDefinitionIndex::WEAPON_KNIFE_BAYONET)
		return;

	CSWeaponType weaponType = activeWeapon->GetCSWpnData()->GetWeaponType();
	if (weaponType == CSWeaponType::WEAPONTYPE_C4 || weaponType == CSWeaponType::WEAPONTYPE_GRENADE)
		return;

	if (activeWeapon->GetNextPrimaryAttack() > globalVars->curtime)
	{
		if (*activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_REVOLVER)
			cmd->buttons &= ~IN_ATTACK2;
		else
			cmd->buttons &= ~IN_ATTACK;
	}
	else
	{
		if (triggerbot_enabled_random_delay && currentTime_ms - oldTimeStamp < randomDelay)
		{
			timeStamp = oldTimeStamp;
			return;
		}

		if (*activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_REVOLVER)
			cmd->buttons |= IN_ATTACK2;
		else
			cmd->buttons |= IN_ATTACK;
		if(triggerbot_enabled_random_delay)
			random_delay_lastroll = randomDelay;

		randomDelay = localMin + rand() % (localMax - localMin);
	}
	timeStamp = currentTime_ms;
}
