#include "customglow.h"

bool custom_glow_enabled = false;
auto custom_glow_allyColor = Color(0, 0, 255, 255);
auto custom_glow_enemyColor = Color(255, 0, 0, 255);
auto custom_glow_enemyVisibleColor = Color(255, 255, 0, 255);
auto custom_glow_localplayerColor = Color(0, 255, 255, 255);
auto custom_glow_weaponColor = Color(158, 158, 158, 255);
auto custom_glow_grenadeColor = Color(96, 125, 139, 255);
auto custom_glow_defuserColor = Color(49, 27, 146, 255);
auto custom_glow_chickenColor = Color(255, 193, 7, 255);
auto custom_glow_droppedc4Color = Color(255, 0, 255, 255);

std::vector<std::pair<int, int>> custom_glow_entities;

void CustomGlow::FrameStageNotify(ClientFrameStage_t stage)
{
	// Skip reserved slots that are guaranteed to be managed by the engine.
	for (int i = 64; i < entityList->GetHighestEntityIndex(); i++)
	{
		C_BaseEntity* entity = entityList->GetClientEntity(i);
		// Register custom entities into the glow object definitions array.
		if (engine->IsInGame() && entity && entity->GetClientClass()->m_ClassID == EClassIds::CBaseAnimating)
		{
			if (!glowManager->HasGlowEffect(entity))
			{
				int array_index = glowManager->RegisterGlowObject(entity);

				if (array_index != -1)
					custom_glow_entities.emplace_back(i, array_index);
			}
		}
		else
		{
			// Remove any entities that no longer exist.
			auto iterator = std::find_if(custom_glow_entities.begin(), custom_glow_entities.end(),
				[&] (const std::pair<int, int>& p) {
					return p.first == i;
				}
			);

			if (iterator != custom_glow_entities.end())
			{
				glowManager->UnregisterGlowObject(iterator->second);
				custom_glow_entities.erase(iterator);
			}
		}
	}
}

static void DrawGlow()
{
	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	if (!localplayer)
		return;

	for (int i = 0; i < glowManager->m_GlowObjectDefinitions.Count(); i++)
	{
		GlowObjectDefinition_t& glow_object = glowManager->m_GlowObjectDefinitions[i];

		if (glow_object.IsUnused() || !glow_object.m_pEntity)
			continue;

		Color color;
		ClientClass* client = glow_object.m_pEntity->GetClientClass();
		bool shouldGlow = true;
		
		if (client->m_ClassID == EClassIds::CCSPlayer) // 40
		{
			C_BasePlayer* player = (C_BasePlayer*) glow_object.m_pEntity;

			if (player->GetDormant() || !player->GetAlive() || !player)
				continue;

			if (player == localplayer)
			{
				color = custom_glow_localplayerColor;
				
			}
			else
			{
				if (glow_object.m_pEntity->GetTeam() != localplayer->GetTeam())
				{
					/*
					Vector src3D, dst3D, forward, src, dst;
					trace_t tr;
					Ray_t ray;
					CTraceFilter filter;
				
					Math::AngleVectors(*player->GetEyeAngles(), forward);
					filter.pSkip = player;
					src3D = player->GetEyePosition();
					dst3D = src3D + (forward * 8192);

					ray.Init(src3D, dst3D); 

					trace->TraceRay(ray, MASK_SHOT, &filter, &tr);

					debugOverlay->AddLineOverlay(src3D, tr.endpos, 255, 255, 0, true, -1.f);
					*/
					if (Entity::IsVisible(player, (int)Bone::BONE_HEAD))
						color = custom_glow_enemyVisibleColor;
					else
						color = custom_glow_enemyColor;
				}
				else
					//color = custom_glow_allyColor.Color(player); // 4
					//color = custom_glow_allyColor;
					continue;
			}
		}
		else if (client->m_ClassID != EClassIds::CBaseWeaponWorldModel &&
				 (strstr(client->m_pNetworkName, XORSTR("Weapon")) || client->m_ClassID == EClassIds::CDEagle || client->m_ClassID == EClassIds::CAK47))
		{
			color = custom_glow_weaponColor;
		}
		else if (client->m_ClassID == EClassIds::CBaseCSGrenadeProjectile || client->m_ClassID == EClassIds::CDecoyProjectile ||
				 client->m_ClassID == EClassIds::CMolotovProjectile || client->m_ClassID == EClassIds::CSmokeGrenadeProjectile)
		{
			color = custom_glow_grenadeColor;
		}
		else if (client->m_ClassID == EClassIds::CBaseAnimating)
		{
			color = custom_glow_defuserColor;

			if (localplayer->HasDefuser() || localplayer->GetTeam() == TeamID::TEAM_TERRORIST)
				shouldGlow = false;
		}
		/*
		else if (client->m_ClassID == EClassIds::CChicken)
		{
			color = custom_glow_chickenColor;

			*reinterpret_cast<C_Chicken*>(glow_object.m_pEntity)->GetShouldGlow() = shouldGlow;
		}
		*/
		else if (client->m_ClassID == EClassIds::CC4)
		{
			color = custom_glow_droppedc4Color;
		}
		else if (client->m_ClassID == EClassIds::CPlantedC4)
		{
			color = custom_glow_droppedc4Color;
		}

		shouldGlow = shouldGlow && color.a > 0;

		glow_object.m_flGlowColor[0] = color.r;
		glow_object.m_flGlowColor[1] = color.g;
		glow_object.m_flGlowColor[2] = color.b;
		glow_object.m_flGlowAlpha = shouldGlow ? color.a : 1.0f;
		glow_object.m_flBloomAmount = 1.0f;
		glow_object.m_bRenderWhenOccluded = shouldGlow;
		glow_object.m_bRenderWhenUnoccluded = false;
	}
}

void CustomGlow::DrawModelExecute(void* thisptr, void* context, void *state, const ModelRenderInfo_t &pInfo, matrix3x4_t* pCustomBoneToWorld)
{
	if (!custom_glow_enabled)
		return;

	if (!engine->IsInGame())
		return;
	DrawGlow();
}
