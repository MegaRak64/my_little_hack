#include "autostrafe.h"
#include "../Utils/math.h"

bool autostrafe_enabled = true;
int autostrafe_type = 1; // 1 - forward; 2 - backward; 3 - leftside; 4 - rightside; 5 - RAGE EPTA
bool autostrafe_silent = false;

static void LegitStrafe(C_BasePlayer* localplayer, CUserCmd* cmd)
{
	if (localplayer->GetFlags() & FL_ONGROUND)
		return;

	if (cmd->buttons & IN_FORWARD || cmd->buttons & IN_BACK || cmd->buttons & IN_MOVELEFT || cmd->buttons & IN_MOVERIGHT)
		return;

	if (cmd->mousedx <= 1 && cmd->mousedx >= -1) // WTF?
		return;

	switch (autostrafe_type)
	{
		// Forward
		case(1):
			cmd->sidemove = cmd->mousedx < 0.f ? -250.f : 250.f; // -250:left 250:right
			break;
		// Backward
		case(2):
			cmd->sidemove = cmd->mousedx < 0.f ? 250.f : -250.f;
			break;
		// Left
		case(3):
			cmd->forwardmove = cmd->mousedx < 0.f ? -250.f : 250.f; // -250:backward 250:forward
			break;
		// Right
		case(4):
			cmd->forwardmove = cmd->mousedx < 0.f ? 250.f : -250.f;
			break;
		default:
			break;
	}
}

static void RageStrafe(C_BasePlayer* localplayer, CUserCmd* cmd)
{
	static bool leftRight;
	bool inMove = cmd->buttons & IN_FORWARD || cmd->buttons & IN_BACK || cmd->buttons & IN_MOVELEFT || cmd->buttons & IN_MOVERIGHT;

	if (cmd->buttons & IN_FORWARD && localplayer->GetVelocity().Length() <= 50.0f)
		cmd->forwardmove = 250.0f;

	float yaw_change = 0.0f;
	if (localplayer->GetVelocity().Length() > 50.f)
		yaw_change = 30.0f * fabsf(30.0f / localplayer->GetVelocity().Length());

	C_BaseCombatWeapon* activeWeapon = (C_BaseCombatWeapon*) entityList->GetClientEntityFromHandle(localplayer->GetActiveWeapon());
	if (activeWeapon && activeWeapon->GetAmmo() == 0 && cmd->buttons & IN_ATTACK)
		yaw_change = 0.0f;

	QAngle viewAngles;
	engine->GetViewAngles(viewAngles);

	if (!(localplayer->GetFlags() & FL_ONGROUND) && !inMove)
	{
		if (leftRight || cmd->mousedx > 1)
		{
			viewAngles.y += yaw_change;
			cmd->sidemove = 250.0f;
		}
		else if (!leftRight || cmd->mousedx < 1)
		{
			viewAngles.y -= yaw_change;
			cmd->sidemove = -250.0f;
		}

		leftRight = !leftRight;
	}

	Math::NormalizeAngles(viewAngles);
	Math::ClampAngles(viewAngles);

	Math::CorrectMovement(viewAngles, cmd, cmd->forwardmove, cmd->sidemove);

	if (!autostrafe_silent)
		cmd->viewangles = viewAngles;
}

void AutoStrafe::CreateMove(CUserCmd* cmd)
{
	if (!autostrafe_enabled)
		return;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	if (!localplayer)
		return;

	if (!localplayer->GetAlive())
		return;

	if (localplayer->GetMoveType() == MOVETYPE_LADDER || localplayer->GetMoveType() == MOVETYPE_NOCLIP)
		return;

	switch (autostrafe_type)
	{
		case(1):
		case(2):
		case(3):
		case(4):
			LegitStrafe(localplayer, cmd);
			break;
		case(5):
			RageStrafe(localplayer, cmd);
			break;
	}
}
