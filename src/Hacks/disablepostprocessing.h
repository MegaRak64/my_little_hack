#pragma once
#include "../hooker.h"

extern bool postprocessing_enabled;

namespace DisablePostProcessing
{
	void BeginFrame();
}
