#pragma once

#include "../interfaces.h"
#include "../Utils/entity.h"
#include "../Utils/math.h"

extern bool triggerbot_enabled;
extern int random_delay_highbound;
extern int random_delay_lowbound;

namespace Triggerbot
{
	//Hooks
	void CreateMove(CUserCmd* cmd);
};
