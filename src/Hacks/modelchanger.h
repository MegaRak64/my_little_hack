#pragma once
#include "../interfaces.h"
#include "../Utils/recvproxyhook.h"
#include "../Utils/netvarmanager.h"

class RecvPropHook;

extern GetLocalClientFn GetLocalClient;

extern std::unordered_map<std::string, std::string> killIcons;

extern bool modelchanger_enabled;

namespace SkinChanger
{
	extern bool forceFullUpdate;
	extern bool glovesUpdated;
	extern std::unique_ptr<RecvPropHook> sequenceHook;

	//Hooks
	void FrameStageNotifySkins(ClientFrameStage_t stage);
	void FrameStageNotifyModels(ClientFrameStage_t stage);
	void FireEventClientSide(IGameEvent* event);
	void FireGameEvent(IGameEvent* event);

	void SetViewModelSequence(const CRecvProxyData *pDataConst, void *pStruct, void *pOut);
};

extern RecvVarProxyFn fnSequenceProxyFn;
