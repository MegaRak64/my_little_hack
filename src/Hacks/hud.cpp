#include "hud.h"
uint8_t key_code_status = 0;
uint8_t submenu_status = 0;
bool submenu_call = false;
uint8_t submenu_blyat = 0;
uint8_t page = 0;

constexpr char* smooth_types[3] = {"Slow end", "Constant", "Fast end"};
constexpr char* antiaim_y[5] = {"NONE", "MAX_DELTA_LEFT", "MAX_DELTA_RIGHT", "MAX_DELTA_FLIPPER", "MAX_DELTA_LBY_AVOID"};
constexpr char* antiaim_x[10] = {"NONE", "STATIC_UP", "STATIC_DOWN", "DANCE", "FRONT", "STATIC_UP_FAKE", "STATIC_DOWN_FAKE", "LISP_DOWN", "ANGEL_DOWN", "ANGEL_UP"};

inline void reverse_bool(bool *parameter)
{
	*parameter = !*parameter;
	key_code_status = 0;
}

void HUD::Paint()
{			
	switch (submenu_status) {
		/* AimBot */
		case (1):
			if (!submenu_call)
			{
				key_code_status = 0;
				submenu_call = true;
			}
			switch (page) {
				/* page 1 */
				case(0):
					switch (key_code_status) {
						case(1):
							reverse_bool(&Settings_AimBot_enabled);
							break;
						case(2):
							reverse_bool(&Settings_AimBot_Smooth_enabled);
							break;
						case(3):
							reverse_bool(&Settings_AimBot_AutoAim_enabled);
							break;
						case(4):
							reverse_bool(&Settings_AimBot_AutoWall_enabled);
							break;
						case(5):
							reverse_bool(&Settings_AimBot_AutoShoot_enabled);
							break;
						case(6):
							reverse_bool(&Settings_AimBot_RCS_enabled);
							break;
						case(7):
							reverse_bool(&Settings_AimBot_RCS_always_on);
							break;
						case(8):
							reverse_bool(&Settings_AimBot_AutoCrouch_enabled);
							break;
						case(9):
							page = 1;
							break;
						default:
							break;
					}
										
					Draw::Text(10, ScreenHeight / 2, Settings_AimBot_enabled ? XORSTR("[1] Статус: Включен") : XORSTR("[1] Статус: Выключен"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 15, Settings_AimBot_Smooth_enabled ? XORSTR("[2] Сглаживание: Включено") : XORSTR("[2] Сглаживание: Выключено"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 30, Settings_AimBot_AutoAim_enabled ? XORSTR("[3] Наводка: Включена") : XORSTR("[3] Наводка: Выключена"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 45, Settings_AimBot_AutoWall_enabled ? XORSTR("[4] Прострел: Включен") : XORSTR("[4] Прострел: Выключен"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 60, Settings_AimBot_AutoShoot_enabled ? XORSTR("[5] Авто-Выстрел: Включен") : XORSTR("[5] Авто-Выстрел: Выключен"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 75, Settings_AimBot_RCS_enabled ? XORSTR("[6] АКО: Включен") : XORSTR("[6] АКО: Выключен"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 90, Settings_AimBot_RCS_always_on ? XORSTR("[7] АКО постоянно: Включен") : XORSTR("[7] АКО постоянно: Выключен"), menu_font, Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 105, Settings_AimBot_AutoCrouch_enabled ? XORSTR("[8] Авто-присед: Включен") : XORSTR("[8] Авто-присед: Выключен"), menu_font, Color(255, 0, 255, 255));
					
					Draw::Text(10, ScreenHeight / 2 + 120, XORSTR("[9] След. страница"), menu_font, Color(255, 0, 255, 255));
					
					Draw::Text(10, ScreenHeight / 2 + 150, XORSTR("[0] Выход"), menu_font, Color(255, 0, 255, 255));
					break;
				/* page 2 */
				case(1):
					switch (submenu_blyat) {
						case(1):
							switch (key_code_status) {
								case(10):
									if (Settings_AimBot_AutoAim_fov < 179.6f)
										Settings_AimBot_AutoAim_fov += 0.5f;
										break;
								case(11):
									if (Settings_AimBot_AutoAim_fov > 0.4f)
										Settings_AimBot_AutoAim_fov -= 0.5f;
									break;
								default:
									break;
							}
							break;
						case(2):
							switch (key_code_status) {
								case(10):
									if (Settings_AimBot_Smooth_value < 1.0f)
										Settings_AimBot_Smooth_value += 0.05f;
										break;
								case(11):
									if (Settings_AimBot_Smooth_value > 0.04f)
										Settings_AimBot_Smooth_value -= 0.05f;
									break;
								default:
									break;
							}
							break;
						case(3):
							switch (key_code_status) {
								case(10):
									Settings_AimBot_Smooth_type > 1 ? Settings_AimBot_Smooth_type = 0 : Settings_AimBot_Smooth_type += 1;
									break;
								case(11):
									Settings_AimBot_Smooth_type < 1 ? Settings_AimBot_Smooth_type = 2 : Settings_AimBot_Smooth_type -= 1;
									break;
								default:
									break;
							}
							break;
						case(4):
							switch (key_code_status) {
								case(10):
									if (Settings_AimBot_AutoWall_value < 100.f)
										Settings_AimBot_AutoWall_value += 5.f;
									break;
								case(11):
									if (Settings_AimBot_AutoWall_value > 14.f)
										Settings_AimBot_AutoWall_value -= 5.f;
									break;
								default:
									break;
							}
							break;
						default:
							break;
					}
					switch (key_code_status) {
						case(1):
							if (!submenu_blyat) 
								submenu_blyat = 1;
							else if (submenu_blyat == 1) 
								submenu_blyat = 0;
							break;
						case(2):
							if (!submenu_blyat) 
								submenu_blyat = 2;
							else if (submenu_blyat == 2) 
								submenu_blyat = 0;
							break;
						case(3):
							if (!submenu_blyat) 
								submenu_blyat = 3;
							else if (submenu_blyat == 3) 
								submenu_blyat = 0;
							break;
						case(4):
							if (!submenu_blyat) 
								submenu_blyat = 4;
							else if (submenu_blyat == 4) 
								submenu_blyat = 0;
							break;
						case(9):
							page = 0;
							break;
						default:
							break;
					}
					
					static char fov[64];
					static char smooth[64];
					static char smooth_type[64];
					static char autowall_damage[64];
					
					sprintf(fov, XORSTR("[1] Обзор наводки: %.1f"), Settings_AimBot_AutoAim_fov);
					sprintf(smooth, XORSTR("[2] Скорость сглаживания: %.2f"), Settings_AimBot_Smooth_value);
					sprintf(smooth_type, XORSTR("[3] Тип сглаживания: %s"), smooth_types[Settings_AimBot_Smooth_type]);
					sprintf(autowall_damage, XORSTR("[4] Урон прострела: %.0f"), Settings_AimBot_AutoWall_value);
					
					Draw::Text(10, ScreenHeight / 2, fov, menu_font, submenu_blyat == 1 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 15, smooth, menu_font, submenu_blyat == 2 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 30, smooth_type, menu_font, submenu_blyat == 3 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
					Draw::Text(10, ScreenHeight / 2 + 45, autowall_damage, menu_font, submenu_blyat == 4 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
					
					Draw::Text(10, ScreenHeight / 2 + 105, XORSTR("[9] Пред. страница"), menu_font, Color(255, 0, 255, 255));
					
					Draw::Text(10, ScreenHeight / 2 + 135, XORSTR("[0] Выход"), menu_font, Color(255, 0, 255, 255));
					break;
			}
			key_code_status = 0;
			break;
		/* TriggerBot */	
		case (2):
			if (!submenu_call)
			{
				key_code_status = 0;
				submenu_call = true;
			}
			switch (submenu_blyat) {
				case(1):
					switch (key_code_status) {
						case(10):
							random_delay_highbound += 10;
							break;
						case(11):
							if ((random_delay_highbound - 10) > random_delay_lowbound)
								random_delay_highbound -= 10;
							break;
						default:
							break;
					}
					break;
				case(2):
					switch (key_code_status) {
						case(10):
							if ((random_delay_lowbound + 10) < random_delay_highbound)
								random_delay_lowbound += 10;
								break;
						case(11):
							if (random_delay_lowbound > 0)
								random_delay_lowbound -= 10;
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
			switch (key_code_status) {
				case(1):
					reverse_bool(&triggerbot_enabled);
					break;
				case(2):
					if (!submenu_blyat) 
						submenu_blyat = 1;
					else if (submenu_blyat == 1) 
						submenu_blyat = 0;
					break;
				case(3):
					if (!submenu_blyat) 
						submenu_blyat = 2;
					else if (submenu_blyat == 2) 
						submenu_blyat = 0;
					break;
				default:
					break;
			}
			
			key_code_status = 0;
			
			Draw::Text(10, ScreenHeight / 2, triggerbot_enabled ? XORSTR("[1] TriggerBot status: Enabled") : XORSTR("[1] TriggerBot status: Disabled"), menu_font, Color(255, 0, 255, 255));

			static char min[32];
			static char max[32];
			
			sprintf(min, XORSTR("[3] TriggerBot min latency: %d"), random_delay_lowbound);
			sprintf(max, XORSTR("[2] TriggerBot max latency: %d"), random_delay_highbound);
			
			Draw::Text(10, ScreenHeight / 2 + 15, max, menu_font, submenu_blyat == 1 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 30, min, menu_font, submenu_blyat == 2 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));

			Draw::Text(10, ScreenHeight / 2 + 60, XORSTR("[0] Exit"), menu_font, Color(255, 0, 255, 255));
			break;
		/* Visuals */
		case (3):
			if (!submenu_call)
			{
				key_code_status = 0;
				submenu_call = true;
			}
			switch (key_code_status) {
				case (1):
					reverse_bool(&chams_enabled);
					break;
				case (2):
					reverse_bool(&custom_glow_enabled);
					break;
				case (3):
					reverse_bool(&esp_enabled);
					break;
				case (4):
					reverse_bool(&postprocessing_enabled);
					break;
				case (5):
					reverse_bool(&recoilcrosshair_enabled);
					break;
			}
			
			Draw::Text(10, ScreenHeight / 2, chams_enabled ? XORSTR("[1] Chams status: Enabled") : XORSTR("[1] Chams status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 15, custom_glow_enabled ? XORSTR("[2] Glow status: Enabled") : XORSTR("[2] Glow status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 30, esp_enabled ? XORSTR("[3] ESP status: Enabled") : XORSTR("[3] ESP status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 45, postprocessing_enabled ? XORSTR("[4] PostProcessing status: Enabled") : XORSTR("[4] PostProcessing status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 60, recoilcrosshair_enabled ? XORSTR("[5] ShowRecoil status: Enabled") : XORSTR("[5] ShowRecoil status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 90, XORSTR("[0] Exit"), menu_font, Color(255, 0, 255, 255));
			break;
		/* Misc */
		case (4):
			if (!submenu_call)
			{
				key_code_status = 0;
				submenu_call = true;
			}
			switch (key_code_status) {
				case (1):
					reverse_bool(&bhop_enabled);
					break;
				case (2):
					reverse_bool(&autostrafe_enabled);
					break;
				case (3):
					reverse_bool(&showranks_enabled);
					break;
			}
			Draw::Text(10, ScreenHeight / 2, bhop_enabled ? XORSTR("[1] BunnyHop status: Enabled") : XORSTR("[1] BunnyHop status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 15, autostrafe_enabled ? XORSTR("[2] AutoStrafe status: Enabled") : XORSTR("[2] AutoStrafe status: Disabled"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 30, showranks_enabled ? XORSTR("[3] ShowRanks status: Enabled") : XORSTR("[3] ShowRanks status: Disabled"), menu_font, Color(255, 0, 255, 255));
			break;
		case (5):
			if (!submenu_call)
			{
				key_code_status = 0;
				submenu_call = true;
			}
			switch (submenu_blyat) {
				case(1):
					switch (key_code_status) {
						case(10):
							Settings_AntiAim_Yaw_type > AntiAimType_Y::MAX_DELTA_FLIPPER ? Settings_AntiAim_Yaw_type = AntiAimType_Y::NONE : Settings_AntiAim_Yaw_type = static_cast<AntiAimType_Y>(static_cast<int>(Settings_AntiAim_Yaw_type)+1);
							break;
						case(11):
							Settings_AntiAim_Yaw_type < AntiAimType_Y::MAX_DELTA_LEFT ? Settings_AntiAim_Yaw_type = AntiAimType_Y::MAX_DELTA_LBY_AVOID : Settings_AntiAim_Yaw_type = static_cast<AntiAimType_Y>(static_cast<int>(Settings_AntiAim_Yaw_type)-1);
							break;
						default:
							break;
					}
					break;
				case(2):
					switch (key_code_status) {
						case(10):
							Settings_AntiAim_Yaw_typeFake > AntiAimType_Y::MAX_DELTA_FLIPPER ? Settings_AntiAim_Yaw_typeFake = AntiAimType_Y::NONE : Settings_AntiAim_Yaw_typeFake = static_cast<AntiAimType_Y>(static_cast<int>(Settings_AntiAim_Yaw_typeFake)+1);
							break;
						case(11):
							Settings_AntiAim_Yaw_typeFake < AntiAimType_Y::MAX_DELTA_LEFT ? Settings_AntiAim_Yaw_typeFake = AntiAimType_Y::MAX_DELTA_LBY_AVOID : Settings_AntiAim_Yaw_typeFake = static_cast<AntiAimType_Y>(static_cast<int>(Settings_AntiAim_Yaw_typeFake)-1);
							break;
						default:
							break;
					}
					break;
				case(3):
					switch (key_code_status) {
						case(10):
							Settings_AntiAim_Pitch_type > AntiAimType_X::ANGEL_DOWN ? Settings_AntiAim_Pitch_type = AntiAimType_X::NONE : Settings_AntiAim_Pitch_type = static_cast<AntiAimType_X>(static_cast<int>(Settings_AntiAim_Pitch_type)+1);
							break;
						case(11):
							Settings_AntiAim_Pitch_type < AntiAimType_X::STATIC_UP ? Settings_AntiAim_Pitch_type = AntiAimType_X::ANGEL_UP : Settings_AntiAim_Pitch_type = static_cast<AntiAimType_X>(static_cast<int>(Settings_AntiAim_Pitch_type)-1);
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
			switch (key_code_status) {
				case(1):
					reverse_bool(&Settings_AntiAim_Yaw_enabled);
					break;
				case(2):
					reverse_bool(&Settings_AntiAim_Pitch_enabled);
					break;
				case(3):
					reverse_bool(&Settings_AntiAim_HeadEdge_enabled);
					break;
				case(4):
					reverse_bool(&Settings_AntiAim_LBYBreaker_enabled);
					break;
				case(5):
					reverse_bool(&Settings_Resolver_resolveAll);
					break;
				case(6):
					if (!submenu_blyat) 
						submenu_blyat = 1;
					else if (submenu_blyat == 1) 
						submenu_blyat = 0;
					break;
				case(7):
					if (!submenu_blyat) 
						submenu_blyat = 2;
					else if (submenu_blyat == 2) 
						submenu_blyat = 0;
					break;
				case(8):
					if (!submenu_blyat) 
						submenu_blyat = 3;
					else if (submenu_blyat == 3) 
						submenu_blyat = 0;
					break;
				default:
					break;
			}
			key_code_status = 0;
			static char yaw_orig[64];
			static char yaw_fake[64];
			static char pitch[64];
			
			sprintf(yaw_orig, XORSTR("[6] Y ось: %s"), antiaim_y[static_cast<int>(Settings_AntiAim_Yaw_type)]);
			sprintf(yaw_fake, XORSTR("[7] Y Fake ось: %s"), antiaim_y[static_cast<int>(Settings_AntiAim_Yaw_typeFake)]);
			sprintf(pitch, XORSTR("[8] X ось: %s"), antiaim_x[static_cast<int>(Settings_AntiAim_Pitch_type)]);
			
			Draw::Text(10, ScreenHeight / 2 - 60, Settings_AntiAim_Yaw_enabled ? XORSTR("[1] Y ось: Включена") : XORSTR("[1] Y ось: Выключена"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 - 45, Settings_AntiAim_Pitch_enabled ? XORSTR("[2] X ось: Включена") : XORSTR("[2] X ось: Выключена"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 - 30, Settings_AntiAim_HeadEdge_enabled ? XORSTR("[3] HE: Включена") : XORSTR("[3] HE: Выключена"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 - 15, Settings_AntiAim_LBYBreaker_enabled ? XORSTR("[4] LBY: Включена") : XORSTR("[4] LBY: Выключена"), menu_font, Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2, Settings_Resolver_resolveAll ? XORSTR("[5] Resolver: Включен") : XORSTR("[5] Resolver: Выключен"), menu_font, Color(255, 0, 255, 255));
			
			Draw::Text(10, ScreenHeight / 2 + 15, yaw_orig, menu_font, submenu_blyat == 1 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 30, yaw_fake, menu_font, submenu_blyat == 2 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 45, pitch, menu_font, submenu_blyat == 3 ? Color(255, 255, 0, 255) : Color(255, 0, 255, 255));
				
			break;
		case (0):
			Draw::Text(10, ScreenHeight / 2 - 30, XORSTR("[1] Аим"), menu_font, Color(0, 255, 255, 255));
			Draw::Text(10, ScreenHeight / 2 - 15, XORSTR("[2] Триггер"), menu_font, Color(0, 255, 255, 255));
			Draw::Text(10, ScreenHeight / 2, XORSTR("[3] Зрение"), menu_font, Color(0, 255, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 15, XORSTR("[4] Остальное"), menu_font, Color(0, 255, 255, 255));
			Draw::Text(10, ScreenHeight / 2 + 45, XORSTR("[5] HvH"), menu_font, Color(0, 255, 255, 255));
			break;
		default:
			break;
	}
}
