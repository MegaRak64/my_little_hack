#include "chams.h"

const char* snipp = "custom\\snip_awp\\awp";

bool chams_enabled = false;

typedef void (*DrawModelExecuteFn) (void*, void*, void*, const ModelRenderInfo_t&, matrix3x4_t*);

/*
static void DrawM4A1S(const ModelRenderInfo_t& pInfo)
{
	std::string modelName = modelInfo->GetModelName(pInfo.pModel);
	IMaterial* mat = material->FindMaterial(XORSTR("custom/rif_m4a1_s/rif_m4a1_s"), XORSTR("Model textures"));
	modelRender->ForcedMaterialOverride(mat);
}
*/
/*
static void DrawAWP(void* thisptr, void* context, void *state, const ModelRenderInfo_t &pInfo, matrix3x4_t* pCustsomBoneToWorld)
{
	static IMaterial* matur = material->FindMaterial("custom/snip_awp/awp", "Model textures");
	IMatRenderContext* contextt = material->GetRenderContext();
	contextt->Bind(matur);
}
*/
/*
static void DrawAK47(const ModelRenderInfo_t& pInfo)
{
	std::string modelName = modelInfo->GetModelName(pInfo.pModel);
	IMaterial* mat = material->FindMaterial(XORSTR("custom/rif_ak47/ak47"), XORSTR("Model textures"));
	modelRender->ForcedMaterialOverride(mat);
	
}
*/
/*
static void DrawP90(const ModelRenderInfo_t& pInfo)
{
	std::string modelName = modelInfo->GetModelName(pInfo.pModel);
	IMaterial* mat = material->FindMaterial(XORSTR("custom/smg_p90/p90"), XORSTR("Model textures"));
	modelRender->ForcedMaterialOverride(mat);
}
*/
void Chams::DrawModelExecute(void* thisptr, void* context, void *state, const ModelRenderInfo_t &pInfo, matrix3x4_t* pCustomBoneToWorld)
{
	
	if (!chams_enabled)
		return;
		
	if (!engine->IsInGame())
		return;

	if (!pInfo.pModel)
		return;

	std::string modelName = modelInfo->GetModelName(pInfo.pModel);
	
	//if (modelName.find(XORSTR("models/weapons/v_snip_awp.mdl")) != std::string::npos)
	//	DrawAWP(thisptr, context, state, pInfo, pCustomBoneToWorld);
	//if (modelName.find(XORSTR("models/weapons/v_rif_ak47.mdl")) != std::string::npos)
	//	DrawAK47(pInfo);
	//if (modelName.find(XORSTR("models/weapons/v_rif_m4a1s.mdl")) != std::string::npos)
	//	DrawM4A1S(pInfo);
	//if (modelName.find(XORSTR("models/weapons/v_smg_p90.mdl")) != std::string::npos)
	//	DrawP90(pInfo);
	
	return;
}
