#pragma once

#include "../interfaces.h"
#include "../Utils/entity.h"
#include "../Utils/math.h"

#include "../SDK/definitions.h"

/* 1 page */
extern bool Settings_AimBot_enabled;
extern bool Settings_AimBot_Smooth_enabled;
extern bool Settings_AimBot_AutoAim_enabled;
extern bool Settings_AimBot_AutoWall_enabled;
extern bool Settings_AimBot_AutoShoot_enabled;
extern bool Settings_AimBot_RCS_enabled;
extern bool Settings_AimBot_RCS_always_on;
extern bool Settings_AimBot_AutoCrouch_enabled;

/* 2 page */
extern float Settings_AimBot_AutoAim_fov;
extern float Settings_AimBot_Smooth_value;
extern int Settings_AimBot_Smooth_type;
extern float Settings_AimBot_AutoWall_value;

extern bool Settings_AimBot_friendly;
extern bool Settings_AimBot_AimStep_enabled;

extern std::vector<int64_t> friends;

/* AutoWall */
extern int AimBot_targetAimbot;
/* AutoWall */
namespace Aimbot
{
	extern bool aimStepInProgress;

	//Hooks
	void CreateMove(CUserCmd* cmd);
	void FireGameEvent(IGameEvent* event);
	void UpdateValues();
}
