#pragma once

#include <math.h>
#include "../Hooks/hooks.h"
#include "../Utils/draw.h"
#include "../interfaces.h"
#include "../Utils/entity.h"
#include "../fonts.h"

#include "hacks.h"

extern uint8_t key_code_status;
extern uint8_t submenu_status;
extern bool submenu_call;
extern uint8_t submenu_blyat;
extern uint8_t page;

constexpr int ScreenWidth = 720;
constexpr int ScreenHeight = 480;

namespace HUD
{
	//Hooks
	void Paint();
}

