#pragma once

#include <math.h>
#include "../Hooks/hooks.h"
#include "../Utils/draw.h"
#include "../interfaces.h"
#include "../Utils/entity.h"
#include "../fonts.h"

extern bool esp_enabled;
extern bool soundesp_enabled;

namespace ESP
{
	void FireGameEvent(IGameEvent* event);
	//Hooks
	bool PrePaintTraverse(VPANEL vgui_panel, bool force_repaint, bool allow_force);
	void Paint();
}

