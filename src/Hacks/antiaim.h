#pragma once

#include "../SDK/IInputSystem.h"
#include "../SDK/IClientEntity.h"
#include "aimbot.h"

enum class AntiAimType_Y : int
{
    NONE = 0,
    MAX_DELTA_LEFT,
    MAX_DELTA_RIGHT,
    MAX_DELTA_FLIPPER,
    MAX_DELTA_LBY_AVOID
};

enum class AntiAimType_X : int
{
    NONE = 0,
    STATIC_UP,
    STATIC_DOWN,
    DANCE,
    FRONT,
    STATIC_UP_FAKE,
    STATIC_DOWN_FAKE,
    LISP_DOWN,
    ANGEL_DOWN,
    ANGEL_UP
};

extern bool Settings_AntiAim_Yaw_enabled;
extern bool Settings_AntiAim_Pitch_enabled;
extern bool Settings_AntiAim_LBYBreaker_enabled;
extern bool Settings_AntiAim_HeadEdge_enabled;

extern AntiAimType_Y Settings_AntiAim_Yaw_type;
extern AntiAimType_Y Settings_AntiAim_Yaw_typeFake;
extern AntiAimType_X Settings_AntiAim_Pitch_type;

namespace AntiAim
{
    extern QAngle realAngle;
    extern QAngle fakeAngle;

    float GetMaxDelta( CCSGOAnimState *animState );

    //Hooks
    void CreateMove(CUserCmd* cmd);
}
