#include "snipercrosshair.h"

#include "../interfaces.h"

#include "../Utils/xorstring.h"
#include "../Utils/draw.h"
#include "../Hooks/hooks.h"

bool Settings_SniperCrosshair_enabled = false;


void SniperCrosshair::Paint( )
{
    if( !esp_enabled || !Settings_SniperCrosshair_enabled )
        return;

    if( !engine->IsInGame() )
        return;

    C_BasePlayer* localPlayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
    if ( !localPlayer || !localPlayer->GetAlive() )
        return;

    C_BaseCombatWeapon *activeWeapon = (C_BaseCombatWeapon*)entityList->GetClientEntityFromHandle(localPlayer->GetActiveWeapon());
    if (!activeWeapon || activeWeapon->GetCSWpnData()->GetWeaponType() != CSWeaponType::WEAPONTYPE_SNIPER_RIFLE)
        return;

    int x = ScreenWidth / 2;
    int y = ScreenHeight / 2;

    // outline horizontal
    Draw::FilledRectangle( x - 4, y - 1, x + 5, y + 2, Color( 0, 0, 0, 170 ) );
    // outline vertical
    Draw::FilledRectangle( x - 1, y - 4, x + 2, y + 5, Color( 0, 0, 0, 170 ) );

    int r = cvar->FindVar(XORSTR("cl_crosshaircolor_r"))->GetInt();
    int g = cvar->FindVar(XORSTR("cl_crosshaircolor_g"))->GetInt();
    int b = cvar->FindVar(XORSTR("cl_crosshaircolor_b"))->GetInt();
    int alpha = cvar->FindVar(XORSTR("cl_crosshairalpha"))->GetInt();
    
    Color crosshair_color = Color(r, g ,b , alpha);

    // line horizontal
    Draw::Line( x - 3, y, x + 4, y, crosshair_color);
    // line vertical
    Draw::Line( x, y + 3, x, y - 4, crosshair_color);
}
