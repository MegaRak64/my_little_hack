#include "bhop.h"

bool bhop_enabled = true;
bool bhop_chance_enabled = false;
int bhop_chance_value = 70;
bool bhop_hops_enabledMax = false;
int bhop_hops_Max = 7;
bool bhop_hops_enabledMin = false;
int bhop_hops_Min = 3;

void BHop::CreateMove(CUserCmd* cmd)
{
	if (!bhop_enabled)
		return;

	static bool bLastJumped = false;
	static bool bShouldFake = false;
	static int bActualHop = 0;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());

	if (!localplayer)
		return;

	if (localplayer->GetMoveType() == MOVETYPE_LADDER || localplayer->GetMoveType() == MOVETYPE_NOCLIP)
		return;


	if (!bLastJumped && bShouldFake)
	{
		bShouldFake = false;
		cmd->buttons |= IN_JUMP;
	}
	else if (cmd->buttons & IN_JUMP)
	{
		if (localplayer->GetFlags() & FL_ONGROUND)
		{
			bActualHop++;
			bLastJumped = true;
			bShouldFake = true;
		}
		else
		{
			if (bhop_chance_enabled &&
				bhop_hops_enabledMin &&
				(bActualHop > bhop_hops_Min) &&
				(rand()%100>bhop_chance_value))
				return;

			if (bhop_chance_enabled &&
				!bhop_hops_enabledMin &&
				(rand()%100 > bhop_chance_value))
				return;

			if (bhop_hops_enabledMin &&
				!bhop_chance_enabled &&
				(bActualHop > bhop_hops_Min))
				return;

			if (bhop_hops_enabledMax &&
				(bActualHop>bhop_hops_Max))
				return;

			cmd->buttons &= ~IN_JUMP;
			bLastJumped = false;
		}
	}
	else
	{
		bActualHop = 0;
		bLastJumped = false;
		bShouldFake = false;
	}
}
