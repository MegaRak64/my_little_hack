#pragma once

#include "../interfaces.h"
#include "../Utils/entity.h"
#include "../Utils/math.h"

extern bool custom_glow_enabled;

namespace CustomGlow
{
	//Hooks
	void FrameStageNotify(ClientFrameStage_t stage);
	void DrawModelExecute(void* thisptr, void* context, void *state, const ModelRenderInfo_t &pInfo, matrix3x4_t* pCustomBoneToWorld);
}
