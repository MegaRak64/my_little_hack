#pragma once

#include "../SDK/SDK.h"
#include "../interfaces.h"
#include "../hooker.h"

namespace PredictionSystem
{
	//Hooks
	/* CreateMove */
	void StartPrediction(CUserCmd* cmd);
	void EndPrediction();
};
