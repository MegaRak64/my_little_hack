#include "aimbot.h"
#include "autowall.h"

#include "../Utils/xorstring.h"
#include "../Utils/math.h"
#include "../Utils/entity.h"
//#include "../Utils/util_items.h"
#include "../interfaces.h"

/* explicit */

Vector Settings_Debug_AutoAim_target = {0, 0, 0};

// Default aimbot settings
bool Settings_AimBot_enabled = true; // false
bool Settings_AimBot_silent = false;
bool Settings_AimBot_friendly = false;
Bone Settings_AimBot_bone = Bone::BONE_HEAD;
ButtonCode_t Settings_AimBot_aimkey = ButtonCode_t::MOUSE_MIDDLE;
bool Settings_AimBot_aimkeyOnly = false;
bool Settings_AimBot_Smooth_enabled = false;
float Settings_AimBot_Smooth_value = 0.5f;
int Settings_AimBot_Smooth_type = 0;
bool Settings_AimBot_ErrorMargin_enabled = false;
float Settings_AimBot_ErrorMargin_value = 0.0f;
bool Settings_AimBot_AutoAim_enabled = true;
float Settings_AimBot_AutoAim_fov = 180.0f;
bool Settings_AimBot_AutoAim_realDistance = false;
bool Settings_AimBot_AutoAim_closestBone = false;
bool Settings_AimBot_AutoAim_desiredBones[] = {true, true, true, true, true, true, true, // center mass
							  false, false, false, false, false, false, false, // left arm
							  false, false, false, false, false, false, false, // right arm
							  false, false, false, false, false, // left leg
							  false, false, false, false, false  // right leg
};
bool Settings_AimBot_AutoAim_engageLock = false;
bool Settings_AimBot_AutoAim_engageLockTR = false; // engage lock Target Reacquisition ( re-target after getting a kill when spraying ).
int Settings_AimBot_AutoAim_engageLockTTR = 700; // Time to Target Reacquisition in ms
bool Settings_AimBot_AutoWall_enabled = true;
float Settings_AimBot_AutoWall_value = 10.0f;
bool Settings_AimBot_AimStep_enabled = false;
float Settings_AimBot_AimStep_min = 25.0f;
float Settings_AimBot_AimStep_max = 35.0f;
bool Settings_AimBot_AutoPistol_enabled = false;
bool Settings_AimBot_AutoShoot_enabled = false;
bool Settings_AimBot_AutoShoot_velocityCheck = false;
bool Settings_AimBot_AutoShoot_autoscope = false;
bool Settings_AimBot_RCS_enabled = true;
bool Settings_AimBot_RCS_always_on = true;
float Settings_AimBot_RCS_valueX = 2.0f;
float Settings_AimBot_RCS_valueY = 2.0f;
bool Settings_AimBot_AutoCrouch_enabled = true;
bool Settings_AimBot_NoShoot_enabled = false;
bool Settings_AimBot_IgnoreJump_enabled = false;
bool Settings_AimBot_IgnoreEnemyJump_enabled = false;
bool Settings_AimBot_SmokeCheck_enabled = false;
bool Settings_AimBot_FlashCheck_enabled = false;
bool Settings_AimBot_SpreadLimit_enabled = true;
float Settings_AimBot_SpreadLimit_value = 0.2f;
bool Settings_AimBot_Smooth_Salting_enabled = false;
float Settings_AimBot_Smooth_Salting_multiplier = 0.0f;
bool Settings_AimBot_AutoSlow_enabled = false;
bool Settings_AimBot_AutoSlow_goingToSlow = false;
bool Settings_AimBot_Prediction_enabled = false;
bool Settings_AimBot_ScopeControl_enabled = false;

bool Aimbot::aimStepInProgress = false;
std::vector<int64_t> friends = { };
std::vector<long> killTimes = { 0 }; // the Epoch time from when we kill someone

bool shouldAim;
QAngle AimStepLastAngle;
QAngle RCSLastPunch;

int AimBot_targetAimbot = -1;
const int headVectors = 11;
/*
std::unordered_map<ItemDefinitionIndex, AimbotWeapon_t, Util::IntHash<ItemDefinitionIndex>> Settings_AimBot_weapons = {
		{ ItemDefinitionIndex::INVALID, defaultSettings },
};
*/
static QAngle ApplyErrorToAngle(QAngle* angles, float margin)
{
	QAngle error;
	error.Random(-1.0f, 1.0f);
	error *= margin;
	angles->operator+=(error);
	return error;
}

/* Fills points Vector. True if successful. False if not.  Credits for Original method - ReactiioN */
static bool HeadMultiPoint(C_BasePlayer *player, Vector points[])
{
	matrix3x4_t matrix[128];

	if( !player->SetupBones(matrix, 128, 0x100, 0.f) )
		return false;
	model_t *pModel = player->GetModel();
	if( !pModel )
		return false;

	studiohdr_t *hdr = modelInfo->GetStudioModel(pModel);
	if( !hdr )
		return false;
	mstudiobbox_t *bbox = hdr->pHitbox(static_cast<int>(Hitbox::HITBOX_HEAD), 0);
	if( !bbox )
		return false;

	Vector mins, maxs;
	Math::VectorTransform(bbox->bbmin, matrix[bbox->bone], mins);
	Math::VectorTransform(bbox->bbmax, matrix[bbox->bone], maxs);

	Vector center = ( mins + maxs ) * 0.5f;
	// 0 - center, 1 - forehead, 2 - skullcap, 3 - upperleftear, 4 - upperrightear, 5 - uppernose, 6 - upperbackofhead
	// 7 - leftear, 8 - rightear, 9 - nose, 10 - backofhead
	for( int i = 0; i < headVectors; i++ ) // set all points initially to center mass of head.
		points[i] = center;
	points[1].z += bbox->radius * 0.60f; // morph each point.
	points[2].z += bbox->radius * 1.25f; // ...
	points[3].x += bbox->radius * 0.80f;
	points[3].z += bbox->radius * 0.60f;
	points[4].x -= bbox->radius * 0.80f;
	points[4].z += bbox->radius * 0.90f;
	points[5].y += bbox->radius * 0.80f;
	points[5].z += bbox->radius * 0.90f;
	points[6].y -= bbox->radius * 0.80f;
	points[6].z += bbox->radius * 0.90f;
	points[7].x += bbox->radius * 0.80f;
	points[8].x -= bbox->radius * 0.80f;
	points[9].y += bbox->radius * 0.80f;
	points[10].y -= bbox->radius * 0.80f;

	return true;
}
static float AutoWallBestSpot(C_BasePlayer *player, Vector &bestSpot)
{
	float bestDamage = Settings_AimBot_AutoWall_value;
	const std::map<int, int> *modelType = Util::GetModelTypeBoneMap(player);

	static int len = sizeof(Settings_AimBot_AutoAim_desiredBones) / sizeof(Settings_AimBot_AutoAim_desiredBones[0]);

	for( int i = 0; i < len; i++ )
	{
		if( !Settings_AimBot_AutoAim_desiredBones[i] )
			continue;
		if( i == static_cast<int>(DesiredBones::BONE_HEAD) ) // head multipoint
		{
			Vector headPoints[headVectors];
			if( !HeadMultiPoint(player, headPoints) )
				continue;
			for( int j = 0; j < headVectors; j++ )
			{
				Autowall::FireBulletData data;
				float spotDamage = Autowall::GetDamage(headPoints[j], !Settings_AimBot_friendly, data);
				
				if( spotDamage > bestDamage )
				{
					bestSpot = headPoints[j];
					if( spotDamage > player->GetHealth() )
						return spotDamage;
					bestDamage = spotDamage;
				}
			}
		}
		int boneID = (*modelType).at(i);
		if( boneID == static_cast<int>(Bone::INVALID) ) // bone not available on this modeltype.
			continue;

		Vector bone3D = player->GetBonePosition(boneID);

		Autowall::FireBulletData data;
		float boneDamage = Autowall::GetDamage(bone3D, !Settings_AimBot_friendly, data);
		// cvar->PRINTEBANAYACONSOL
		if( boneDamage > bestDamage )
		{
			bestSpot = bone3D;
			if( boneDamage > player->GetHealth() )
				return boneDamage;

			bestDamage = boneDamage;
		}
	}
	return bestDamage;
}

static float GetRealDistanceFOV(float distance, QAngle angle, CUserCmd* cmd)
{
	/*    n
	    w + e
	      s        'real distance'
	                      |
	   a point -> x --..  v
	              |     ''-- x <- a guy
	              |          /
	             |         /
	             |       /
	            | <------------ both of these lines are the same length
	            |    /      /
	           |   / <-----'
	           | /
	          o
	     localplayer
	*/

	Vector aimingAt;
	Math::AngleVectors(cmd->viewangles, aimingAt);
	aimingAt *= distance;

	Vector aimAt;
	Math::AngleVectors(angle, aimAt);
	aimAt *= distance;

	return aimingAt.DistTo(aimAt);
}

static Vector VelocityExtrapolate(C_BasePlayer* player, Vector aimPos)
{
	return aimPos + (player->GetVelocity() * globalVars->interval_per_tick);
}

/* Original Credits to: https://github.com/goldenguy00 ( study! study! study! :^) ) */

/*
 * 1 - FOV
 * 2 - REAL DISTANCE
*/
static Vector GetClosestSpot( CUserCmd* cmd, C_BasePlayer* localPlayer, C_BasePlayer* enemy, int aimTargetType = 1)
{
	QAngle viewAngles;
	engine->GetViewAngles(viewAngles);

	float tempFov = Settings_AimBot_AutoAim_fov;
	float tempDistance = Settings_AimBot_AutoAim_fov * 5.f;

	Vector pVecTarget = localPlayer->GetEyePosition();

	Vector tempSpot = {0,0,0};

	const std::map<int, int> *modelType = Util::GetModelTypeBoneMap(enemy);

	static int len = sizeof(Settings_AimBot_AutoAim_desiredBones) / sizeof(Settings_AimBot_AutoAim_desiredBones[0]);
	for( int i = 0; i < len; i++ )
	{
		if( !Settings_AimBot_AutoAim_desiredBones[i] )
			continue;

		int boneID = (*modelType).at(i);
		if( boneID == static_cast<int>(Bone::INVALID) )
			continue;

		Vector cbVecTarget = enemy->GetBonePosition(boneID);

		if( aimTargetType == 1 )
		{
			float cbFov = Math::GetFov(viewAngles, Math::CalcAngle(pVecTarget, cbVecTarget));

			if( cbFov < tempFov )
			{
				if( Entity::IsVisibleThroughEnemies(enemy, boneID) )
				{
					tempFov = cbFov;
					tempSpot = cbVecTarget;
				}
			}
		}
		else if( aimTargetType == 2 )
		{
			float cbDistance = pVecTarget.DistTo(cbVecTarget);
			float cbRealDistance = GetRealDistanceFOV(cbDistance, Math::CalcAngle(pVecTarget, cbVecTarget), cmd);

			if( cbRealDistance < tempDistance )
			{
				if( Entity::IsVisibleThroughEnemies(enemy, boneID) )
				{
					tempDistance = cbRealDistance;
					tempSpot = cbVecTarget;
				}
			}
		}
	}
	return tempSpot;
}

static C_BasePlayer* GetClosestPlayerAndSpot(CUserCmd* cmd, bool visibleCheck, Vector* bestSpot, float* bestDamage, int aimTargetType = 1)
{
	if (Settings_AimBot_AutoAim_realDistance)
		aimTargetType = 2;

	static C_BasePlayer* lockedOn = nullptr;
	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	C_BasePlayer* closestEntity = nullptr;

	float bestFov = Settings_AimBot_AutoAim_fov;
	float bestRealDistance = Settings_AimBot_AutoAim_fov * 5.f;

	if( lockedOn )
	{
		if( lockedOn->GetAlive() && !Settings_AimBot_AutoAim_closestBone && !Entity::IsSpotVisibleThroughEnemies(lockedOn, lockedOn->GetBonePosition(static_cast<int>(Settings_AimBot_bone))) )
		{
			lockedOn = nullptr;
			return nullptr;
		}
		if (!(cmd->buttons & IN_ATTACK || inputSystem->IsButtonDown(Settings_AimBot_aimkey)) || lockedOn->GetDormant())//|| !Entity::IsVisible(lockedOn, bestBone, 180.f, Settings_ESP::Filters::smokeCheck))
		{
			lockedOn = nullptr;
		}
		else
		{
			if( !lockedOn->GetAlive() )
			{
				if( Settings_AimBot_AutoAim_engageLockTR )
				{
					if(Util::GetEpochTime() - killTimes.back() > Settings_AimBot_AutoAim_engageLockTTR) // if we got the kill over the TTR time, engage another foe.
					{
						lockedOn = nullptr;
					}
				}
				return nullptr;
			}

			if( Settings_AimBot_AutoAim_closestBone )
			{
				Vector tempSpot = GetClosestSpot(cmd, localplayer, lockedOn, aimTargetType);
				if( tempSpot.IsZero() )
				{
					return nullptr;
				}
				*bestSpot = tempSpot;
			}
			else
			{
				*bestSpot = lockedOn->GetBonePosition(static_cast<int>(Settings_AimBot_bone));
			}

			return lockedOn;
		}
	}

	for (int i = 1; i < engine->GetMaxClients(); ++i)
	{
		C_BasePlayer* player = (C_BasePlayer*) entityList->GetClientEntity(i);

		if (!player
			|| player == localplayer
			|| player->GetDormant()
			|| !player->GetAlive()
			|| player->GetImmune()
			|| player->GetTeam() == localplayer->GetTeam())
			continue;
		/* reconstruction */
		//if (!Settings_AimBot_friendly /*&& Entity::IsTeamMate(player, localplayer)*/)
		//	continue;

		if( !friends.empty() ) // check for friends, if any
		{
			IEngineClient::player_info_t entityInformation;
			engine->GetPlayerInfo(i, &entityInformation);

			if (std::find(friends.begin(), friends.end(), entityInformation.xuid) != friends.end())
				continue;
		}

		AimBot_targetAimbot = i;
		Vector eVecTarget = player->GetBonePosition(static_cast<int>(Settings_AimBot_bone));
		if( Settings_AimBot_AutoAim_closestBone )
		{
			Vector tempSpot = GetClosestSpot(cmd, localplayer, player, aimTargetType);
			if( tempSpot.IsZero() || !Entity::IsSpotVisibleThroughEnemies(player, tempSpot) )
				continue;
			eVecTarget = tempSpot;
		}

		Vector pVecTarget = localplayer->GetEyePosition();
        lastRayStart = pVecTarget;
        lastRayEnd = eVecTarget;

		QAngle viewAngles;
		engine->GetViewAngles(viewAngles);

		float distance = pVecTarget.DistTo(eVecTarget);
		float fov = Math::GetFov(viewAngles, Math::CalcAngle(pVecTarget, eVecTarget));

		if (aimTargetType == 1 && fov > bestFov)
			continue;

		float realDistance = GetRealDistanceFOV(distance, Math::CalcAngle(pVecTarget, eVecTarget), cmd);

		if (aimTargetType == 2 && realDistance > bestRealDistance)
			continue;
		if (visibleCheck && !Settings_AimBot_AutoWall_enabled && !Entity::IsSpotVisible(player, eVecTarget))
			continue;
		if ( Settings_AimBot_SmokeCheck_enabled && LineGoesThroughSmoke( localplayer->GetEyePosition( ), eVecTarget, true ) )
			continue;
		if ( Settings_AimBot_FlashCheck_enabled && localplayer->IsFlashed() )
			continue;

		if (Settings_AimBot_AutoWall_enabled)
		{
			Vector wallBangSpot = {0,0,0};
			float damage = AutoWallBestSpot(player, wallBangSpot); // sets Vector Angle, returns damage of hitting that spot.

			if( !wallBangSpot.IsZero() )
			{
				*bestDamage = damage;
				*bestSpot = wallBangSpot;
				closestEntity = player;
				lastRayEnd = wallBangSpot;
			}
		}
		else
		{
			closestEntity = player;
			*bestSpot = eVecTarget;
			bestFov = fov;
			bestRealDistance = realDistance;
		}
	}
	if( Settings_AimBot_AutoAim_engageLock )
	{
		if( !lockedOn )
		{
			if( (cmd->buttons & IN_ATTACK) || inputSystem->IsButtonDown(Settings_AimBot_aimkey) )
			{
				if( Util::GetEpochTime() - killTimes.back() > 100 ) // if we haven't gotten a kill in under 100ms.
				{
					lockedOn = closestEntity; // This is to prevent a Rare condition when you one-tap someone without the aimbot, it will lock on to another target.
				}
			}
			else
			{
				return nullptr;
			}
		}
	}
	if( bestSpot->IsZero() )
		return nullptr;

	/*
	if( closestEntity )
	{
		IEngineClient::player_info_t playerInfo;
		engine->GetPlayerInfo(closestEntity->GetIndex(), &playerInfo);
		cvar->ConsoleDPrintf("%s is Closest.\n", playerInfo.name);
	}
	*/

	return closestEntity;
}

static void RCS(QAngle& angle, C_BasePlayer* player, CUserCmd* cmd)
{
	if (!Settings_AimBot_RCS_enabled)
		return;

	if (!(cmd->buttons & IN_ATTACK))
		return;

	bool hasTarget = Settings_AimBot_AutoAim_enabled && shouldAim && player;

	if (!Settings_AimBot_RCS_always_on && !hasTarget)
		return;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	QAngle CurrentPunch = *localplayer->GetAimPunchAngle();

	if ( Settings_AimBot_silent || hasTarget )
	{
		angle.x -= CurrentPunch.x * Settings_AimBot_RCS_valueX;
		angle.y -= CurrentPunch.y * Settings_AimBot_RCS_valueY;
	}
	else if (localplayer->GetShotsFired() > 1)
	{
		QAngle NewPunch = { CurrentPunch.x - RCSLastPunch.x, CurrentPunch.y - RCSLastPunch.y, 0 };

		angle.x -= NewPunch.x * Settings_AimBot_RCS_valueX;
		angle.y -= NewPunch.y * Settings_AimBot_RCS_valueY;
	}

	RCSLastPunch = CurrentPunch;
}
static void AimStep(C_BasePlayer* player, QAngle& angle, CUserCmd* cmd)
{
	if (!Settings_AimBot_AimStep_enabled)
		return;

	if (!Settings_AimBot_AutoAim_enabled)
		return;

	if (Settings_AimBot_Smooth_enabled)
		return;

	if (!shouldAim)
		return;

	if (!Aimbot::aimStepInProgress)
		AimStepLastAngle = cmd->viewangles;

	if (!player)
		return;

	float fov = Math::GetFov(AimStepLastAngle, angle);

	Aimbot::aimStepInProgress = ( fov > (Math::float_rand(Settings_AimBot_AimStep_min, Settings_AimBot_AimStep_max)) );

	if (!Aimbot::aimStepInProgress)
		return;

    cmd->buttons &= ~(IN_ATTACK); // aimstep in progress, don't shoot.

	QAngle deltaAngle = AimStepLastAngle - angle;

	Math::NormalizeAngles(deltaAngle);
	float randX = Math::float_rand(Settings_AimBot_AimStep_min, std::min(Settings_AimBot_AimStep_max, fov));
	float randY = Math::float_rand(Settings_AimBot_AimStep_min, std::min(Settings_AimBot_AimStep_max, fov));
	if (deltaAngle.y < 0)
		AimStepLastAngle.y += randY;
	else
		AimStepLastAngle.y -= randY;

	if(deltaAngle.x < 0)
		AimStepLastAngle.x += randX;
	else
		AimStepLastAngle.x -= randX;

	angle = AimStepLastAngle;
}

static void Salt(float& smooth)
{
	float sine = sin (globalVars->tickcount);
	float salt = sine * Settings_AimBot_Smooth_Salting_multiplier;
	float oval = smooth + salt;
	smooth *= oval;
}

static void Smooth(C_BasePlayer* player, QAngle& angle)
{
	if (!Settings_AimBot_Smooth_enabled)
		return;
	if (!shouldAim || !player)
		return;
	if (Settings_AimBot_silent)
		return;

	QAngle viewAngles;
	engine->GetViewAngles(viewAngles);

	QAngle delta = angle - viewAngles;
	Math::NormalizeAngles(delta);

	float smooth = powf(Settings_AimBot_Smooth_value, 0.4f); // Makes more slider space for actual useful values

	smooth = std::min(0.99f, smooth);

	if (Settings_AimBot_Smooth_Salting_enabled)
		Salt(smooth);

	QAngle toChange = {0,0,0};
	
	// WTF?
	
	// excess variable
	//SmoothType type = Settings_AimBot_Smooth_type;
	
	/* 
	 * SLOW_END - 0
	 * CONSTANT - 1
	 * FAST_END - 2
	*/
	
	if (!Settings_AimBot_Smooth_type)
		toChange = delta - (delta * smooth);
	else if (Settings_AimBot_Smooth_type)
	{
		float coeff = (1.0f - smooth) / delta.Length() * 4.f;

		if (Settings_AimBot_Smooth_type == 2)
			coeff = powf(coeff, 2.f) * 10.f;

		coeff = std::min(1.f, coeff);
		toChange = delta * coeff;
	}

	angle = viewAngles + toChange;
}

static void AutoCrouch(C_BasePlayer* player, CUserCmd* cmd)
{
	if (!Settings_AimBot_AutoCrouch_enabled)
		return;

	if (!player)
		return;

	cmd->buttons |= IN_BULLRUSH | IN_DUCK;
}

static void AutoSlow(C_BasePlayer* player, float& forward, float& sideMove, float& bestDamage, C_BaseCombatWeapon* active_weapon, CUserCmd* cmd)
{

	if (!Settings_AimBot_AutoSlow_enabled){
		Settings_AimBot_AutoSlow_goingToSlow = false;
		return;
	}

	if (!player){
		Settings_AimBot_AutoSlow_goingToSlow = false;
		return;
	}

	float nextPrimaryAttack = active_weapon->GetNextPrimaryAttack();

	if (nextPrimaryAttack > globalVars->curtime){
		Settings_AimBot_AutoSlow_goingToSlow = false;
		return;
	}

	Settings_AimBot_AutoSlow_goingToSlow = true;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());

	C_BaseCombatWeapon* activeWeapon = (C_BaseCombatWeapon*) entityList->GetClientEntityFromHandle(localplayer->GetActiveWeapon());
	if (!activeWeapon || activeWeapon->GetAmmo() == 0)
		return;

	if( Settings_AimBot_SpreadLimit_enabled )
	{
		if( (activeWeapon->GetSpread() + activeWeapon->GetInaccuracy()) > Settings_AimBot_SpreadLimit_value )
		{
			cmd->buttons |= IN_WALK;
			forward = -forward;
			sideMove = -sideMove;
			cmd->upmove = 0;
		}
	}
	else if( localplayer->GetVelocity().Length() > (activeWeapon->GetCSWpnData()->GetMaxPlayerSpeed() / 3) ) // https://youtu.be/ZgjYxBRuagA
	{
		cmd->buttons |= IN_WALK;
		forward = -forward;
		sideMove = -sideMove;
		cmd->upmove = 0;
	}
}

static void AutoCock(C_BasePlayer* player, C_BaseCombatWeapon* activeWeapon, CUserCmd* cmd)
{
    if (!Settings_AimBot_AutoShoot_enabled)
        return;

    if (Settings_AimBot_AimStep_enabled && Aimbot::aimStepInProgress)
        return;

    if (*activeWeapon->GetItemDefinitionIndex() != ItemDefinitionIndex::WEAPON_REVOLVER)
        return;

    if(activeWeapon->GetAmmo() == 0)
        return;
    if (cmd->buttons & IN_USE)
        return;

    cmd->buttons |= IN_ATTACK;
    float postponeFireReadyTime = activeWeapon->GetPostPoneReadyTime();
    if (postponeFireReadyTime > 0)
    {
        if (postponeFireReadyTime < globalVars->curtime)
        {
            if (player)
                return;
            cmd->buttons &= ~IN_ATTACK;
        }
    }
}

static void AutoPistol(C_BaseCombatWeapon* activeWeapon, CUserCmd* cmd)
{
	if (!Settings_AimBot_AutoPistol_enabled)
		return;

	if (!activeWeapon || activeWeapon->GetCSWpnData()->GetWeaponType() != CSWeaponType::WEAPONTYPE_PISTOL)
		return;

	if (activeWeapon->GetNextPrimaryAttack() < globalVars->curtime)
		return;

    if (*activeWeapon->GetItemDefinitionIndex() != ItemDefinitionIndex::WEAPON_REVOLVER)
        cmd->buttons &= ~IN_ATTACK;
}

static void AutoShoot(C_BasePlayer* player, C_BaseCombatWeapon* activeWeapon, CUserCmd* cmd)
{
	if (!Settings_AimBot_AutoShoot_enabled)
		return;

	if (Settings_AimBot_AimStep_enabled && Aimbot::aimStepInProgress)
		return;

	if (!player || activeWeapon->GetAmmo() == 0)
		return;

	CSWeaponType weaponType = activeWeapon->GetCSWpnData()->GetWeaponType();
	if (weaponType == CSWeaponType::WEAPONTYPE_KNIFE || weaponType == CSWeaponType::WEAPONTYPE_C4 || weaponType == CSWeaponType::WEAPONTYPE_GRENADE)
		return;

	if (cmd->buttons & IN_USE)
		return;

	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());

	if (Settings_AimBot_AutoShoot_autoscope)
		if ((weaponType == CSWeaponType::WEAPONTYPE_SNIPER_RIFLE || *activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_AUG ||
		 *activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_SG556) && !localplayer->IsScoped())
			cmd->buttons |= IN_ATTACK2;

	if( Settings_AimBot_AutoShoot_velocityCheck && localplayer->GetVelocity().Length() > (activeWeapon->GetCSWpnData()->GetMaxPlayerSpeed() / 3) )
		return;
	if( Settings_AimBot_SpreadLimit_enabled && ((activeWeapon->GetSpread() + activeWeapon->GetInaccuracy()) > Settings_AimBot_SpreadLimit_value))
		return;

	float nextPrimaryAttack = activeWeapon->GetNextPrimaryAttack();

    if (!(*activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_REVOLVER))
    {
        if (nextPrimaryAttack > globalVars->curtime)
            cmd->buttons &= ~IN_ATTACK;
        else
            cmd->buttons |= IN_ATTACK;
    }
}

static void NoShoot(C_BaseCombatWeapon* activeWeapon, C_BasePlayer* player, CUserCmd* cmd)
{
	if (player && Settings_AimBot_NoShoot_enabled)
	{
		if (*activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_C4)
			return;

		if (*activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_REVOLVER)
			cmd->buttons &= ~IN_ATTACK2;
		else
			cmd->buttons &= ~IN_ATTACK;
	}
}

static void FixMouseDeltas(CUserCmd* cmd, const QAngle &angle, const QAngle &oldAngle)
{
    if( !shouldAim )
        return;
    QAngle delta = angle - oldAngle;
    float sens = cvar->FindVar(XORSTR("sensitivity"))->GetFloat();
    float m_pitch = cvar->FindVar(XORSTR("m_pitch"))->GetFloat();
    float m_yaw = cvar->FindVar(XORSTR("m_yaw"))->GetFloat();
    float zoomMultiplier = cvar->FindVar("zoom_sensitivity_ratio_mouse")->GetFloat();

    Math::NormalizeAngles(delta);

    cmd->mousedx = -delta.y / ( m_yaw * sens * zoomMultiplier );
    cmd->mousedy = delta.x / ( m_pitch * sens * zoomMultiplier );
}
void Aimbot::CreateMove(CUserCmd* cmd)
{
	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	if (!localplayer || !localplayer->GetAlive())
		return;

	//AimBot_UpdateValues();

	if (!Settings_AimBot_enabled)
		return;

	QAngle oldAngle;
	engine->GetViewAngles(oldAngle);
	float oldForward = cmd->forwardmove;
	float oldSideMove = cmd->sidemove;

	QAngle angle = cmd->viewangles;
	static bool newTarget = true;
	static QAngle lastRandom = {0,0,0};
	Vector localEye = localplayer->GetEyePosition();

	shouldAim = Settings_AimBot_AutoShoot_enabled;

	if (Settings_AimBot_IgnoreJump_enabled && (!(localplayer->GetFlags() & FL_ONGROUND) && localplayer->GetMoveType() != MOVETYPE_LADDER))
		return;

	C_BaseCombatWeapon* activeWeapon = (C_BaseCombatWeapon*) entityList->GetClientEntityFromHandle(localplayer->GetActiveWeapon());
	if (!activeWeapon || activeWeapon->GetInReload())
		return;

	CSWeaponType weaponType = activeWeapon->GetCSWpnData()->GetWeaponType();
	if (weaponType == CSWeaponType::WEAPONTYPE_C4 || weaponType == CSWeaponType::WEAPONTYPE_GRENADE || weaponType == CSWeaponType::WEAPONTYPE_KNIFE)
		return;

	if (Settings_AimBot_ScopeControl_enabled)
	{
		if ((weaponType == CSWeaponType::WEAPONTYPE_SNIPER_RIFLE || *activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_AUG ||
		 *activeWeapon->GetItemDefinitionIndex() == ItemDefinitionIndex::WEAPON_SG556) && !localplayer->IsScoped())
			return;
	}

    Vector bestSpot = {0,0,0};
	float bestDamage = 0.0f;
	C_BasePlayer* player = GetClosestPlayerAndSpot(cmd, !Settings_AimBot_AutoWall_enabled, &bestSpot, &bestDamage);

	if (player)
	{
		if (Settings_AimBot_IgnoreEnemyJump_enabled && (!(player->GetFlags() & FL_ONGROUND) && player->GetMoveType() != MOVETYPE_LADDER))
			return;

		if (Settings_AimBot_AutoAim_enabled)
		{
			if (cmd->buttons & IN_ATTACK && !Settings_AimBot_aimkeyOnly)
				shouldAim = true;

			if (inputSystem->IsButtonDown(Settings_AimBot_aimkey))
				shouldAim = true;

			Settings_Debug_AutoAim_target = bestSpot; // For Debug showing aimspot.
			if (shouldAim)
			{
				if (Settings_AimBot_Prediction_enabled)
				{
					localEye = VelocityExtrapolate(localplayer, localEye); // get eye pos next tick
					bestSpot = VelocityExtrapolate(player, bestSpot); // get target pos next tick
				}
				angle = Math::CalcAngle(localEye, bestSpot);

				if (Settings_AimBot_ErrorMargin_enabled)
				{
					static int lastShotFired = 0;
					if ((localplayer->GetShotsFired() > lastShotFired) || newTarget) //get new random spot when firing a shot or when aiming at a new target
						lastRandom = ApplyErrorToAngle(&angle, Settings_AimBot_ErrorMargin_value);

					angle += lastRandom;
					lastShotFired = localplayer->GetShotsFired();
				}
				newTarget = false;
			}
		}
	}
	else // No player to Shoot
	{
        Settings_Debug_AutoAim_target = {0,0,0};
        newTarget = true;
        lastRandom = {0,0,0};
    }

    AimStep(player, angle, cmd);
	AutoCrouch(player, cmd);
	AutoSlow(player, oldForward, oldSideMove, bestDamage, activeWeapon, cmd);
	AutoPistol(activeWeapon, cmd);
	AutoShoot(player, activeWeapon, cmd);
	AutoCock(player, activeWeapon, cmd);
	RCS(angle, player, cmd);
	Smooth(player, angle);
	NoShoot(activeWeapon, player, cmd);

    Math::NormalizeAngles(angle);
    Math::ClampAngles(angle);

	FixMouseDeltas(cmd, angle, oldAngle);
	cmd->viewangles = angle;

    Math::CorrectMovement(oldAngle, cmd, oldForward, oldSideMove);

	if( !Settings_AimBot_silent )
    	engine->SetViewAngles(cmd->viewangles);
}
void Aimbot::FireGameEvent(IGameEvent* event)
{
	if (!event)
		return;

	if (strcmp(event->GetName(), XORSTR("player_connect_full")) == 0 || strcmp(event->GetName(), XORSTR("cs_game_disconnected")) == 0 )
	{
		if (event->GetInt(XORSTR("userid")) && engine->GetPlayerForUserID(event->GetInt(XORSTR("userid"))) != engine->GetLocalPlayer())
			return;
		friends.clear();
	}
	if( strcmp(event->GetName(), XORSTR("player_death")) == 0 )
	{
		int attacker_id = engine->GetPlayerForUserID(event->GetInt(XORSTR("attacker")));
		int deadPlayer_id = engine->GetPlayerForUserID(event->GetInt(XORSTR("userid")));

		if (attacker_id == deadPlayer_id) // suicide
			return;

		if (attacker_id != engine->GetLocalPlayer())
			return;

		killTimes.push_back(Util::GetEpochTime());
	}
}
// We use global variable for all weapons, no specified
/*
void AimBot_UpdateValues()
{
	C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	C_BaseCombatWeapon* activeWeapon = (C_BaseCombatWeapon*) entityList->GetClientEntityFromHandle(localplayer->GetActiveWeapon());
	if (!activeWeapon)
		return;

	ItemDefinitionIndex index = ItemDefinitionIndex::INVALID;
	if (Settings_AimBot_weapons.find(*activeWeapon->GetItemDefinitionIndex()) != Settings_AimBot_weapons.end())
		index = *activeWeapon->GetItemDefinitionIndex();

	const AimbotWeapon_t& currentWeaponSetting = Settings_AimBot_weapons.at(index);

	Settings_AimBot_enabled = currentWeaponSetting.enabled;
	Settings_AimBot_silent = currentWeaponSetting.silent;
	Settings_AimBot_friendly = currentWeaponSetting.friendly;
	Settings_AimBot_bone = currentWeaponSetting.bone;
	Settings_AimBot_aimkey = currentWeaponSetting.aimkey;
	Settings_AimBot_aimkeyOnly = currentWeaponSetting.aimkeyOnly;
	Settings_AimBot_Smooth_enabled = currentWeaponSetting.smoothEnabled;
	Settings_AimBot_Smooth_value = currentWeaponSetting.smoothAmount;
	Settings_AimBot_Smooth_type = currentWeaponSetting.smoothType;
	Settings_AimBot_ErrorMargin_enabled = currentWeaponSetting.errorMarginEnabled;
	Settings_AimBot_ErrorMargin_value = currentWeaponSetting.errorMarginValue;
	Settings_AimBot_AutoAim_enabled = currentWeaponSetting.autoAimEnabled;
	Settings_AimBot_AutoAim_fov = currentWeaponSetting.autoAimFov;
	Settings_AimBot_AutoAim_closestBone = currentWeaponSetting.closestBone;
	Settings_AimBot_AutoAim_engageLock = currentWeaponSetting.engageLock;
	Settings_AimBot_AutoAim_engageLockTR = currentWeaponSetting.engageLockTR;
	Settings_AimBot_AutoAim_engageLockTTR = currentWeaponSetting.engageLockTTR;
	Settings_AimBot_AimStep_enabled = currentWeaponSetting.aimStepEnabled;
	Settings_AimBot_AimStep_min = currentWeaponSetting.aimStepMin;
	Settings_AimBot_AimStep_max = currentWeaponSetting.aimStepMax;
	Settings_AimBot_AutoPistol_enabled = currentWeaponSetting.autoPistolEnabled;
	Settings_AimBot_AutoShoot_enabled = currentWeaponSetting.autoShootEnabled;
	Settings_AimBot_AutoShoot_autoscope = currentWeaponSetting.autoScopeEnabled;
	Settings_AimBot_RCS_enabled = currentWeaponSetting.rcsEnabled;
	Settings_AimBot_RCS_always_on = currentWeaponSetting.rcsAlwaysOn;
	Settings_AimBot_RCS_valueX = currentWeaponSetting.rcsAmountX;
	Settings_AimBot_RCS_valueY = currentWeaponSetting.rcsAmountY;
	Settings_AimBot_NoShoot_enabled = currentWeaponSetting.noShootEnabled;
	Settings_AimBot_IgnoreJump_enabled = currentWeaponSetting.ignoreJumpEnabled;
	Settings_AimBot_IgnoreEnemyJump_enabled = currentWeaponSetting.ignoreEnemyJumpEnabled;
	Settings_AimBot_Smooth_Salting_enabled = currentWeaponSetting.smoothSaltEnabled;
	Settings_AimBot_Smooth_Salting_multiplier = currentWeaponSetting.smoothSaltMultiplier;
	Settings_AimBot_SmokeCheck_enabled = currentWeaponSetting.smokeCheck;
	Settings_AimBot_FlashCheck_enabled = currentWeaponSetting.flashCheck;
	Settings_AimBot_SpreadLimit_enabled = currentWeaponSetting.spreadLimitEnabled;
	Settings_AimBot_SpreadLimit_value = currentWeaponSetting.spreadLimit;
	Settings_AimBot_AutoWall_enabled = currentWeaponSetting.autoWallEnabled;
	Settings_AimBot_AutoWall_value = currentWeaponSetting.autoWallValue;
	Settings_AimBot_AutoSlow_enabled = currentWeaponSetting.autoSlow;
	Settings_AimBot_ScopeControl_enabled = currentWeaponSetting.scopeControlEnabled;

	for (int bone = static_cast<int>(DesiredBones::BONE_PELVIS); bone <= static_cast<int>(DesiredBones::BONE_RIGHT_SOLE); bone++)
		Settings_AimBot_AutoAim_desiredBones[bone] = currentWeaponSetting.desiredBones[bone];

	Settings_AimBot_AutoAim_realDistance = currentWeaponSetting.autoAimRealDistance;
}
*/
