#include "showranks.h"

bool showranks_enabled = false;

void ShowRanks::CreateMove(CUserCmd* cmd)
{
	if (!showranks_enabled)
		return;

	if (!(cmd->buttons & IN_SCORE))
		return;

	float input[3] = { 0.f };
	//if (MsgFunc_ServerRankRevealAll)
		MsgFunc_ServerRankRevealAll(input);
}
