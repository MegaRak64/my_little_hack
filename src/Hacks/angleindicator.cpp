#include "angleindicator.h"

#include "../interfaces.h"
#include "antiaim.h"
#include "../Utils/draw.h"
#include "../Utils/math.h"
#include "../Hooks/hooks.h"
#include "hud.h"

bool Settings_AngleIndicator_enabled = true;

void AngleIndicator::Paint( ) {
    if( !Settings_AntiAim_Yaw_enabled || !engine->IsInGame() )
        return;

    C_BasePlayer* localPlayer = ( C_BasePlayer* ) entityList->GetClientEntity( engine->GetLocalPlayer() );

    if ( !localPlayer || !localPlayer->GetAlive() )
        return;

    int radius = 55;
    constexpr int centerX = ScreenWidth / 3;
    constexpr int centerY = ScreenHeight / 2;
    int northY = centerY - radius;

    float maxDesync = AntiAim::GetMaxDelta( localPlayer->GetAnimState() );
    float realDiff = AntiAim::fakeAngle.y - AntiAim::realAngle.y;
    float lbyDiff = localPlayer->GetVAngles()->y - *localPlayer->GetLowerBodyYawTarget();
    Math::NormalizeYaw( realDiff );
    Math::NormalizeYaw( lbyDiff );

    int leftDesyncMaxX = radius * cos( DEG2RAD( 90 + maxDesync ) ) + centerX;
    int leftDesyncMaxY = centerY - ( radius * sin( DEG2RAD( 90 + maxDesync ) ) );
    int rightDesyncMaxX = radius * cos( DEG2RAD( 90 - maxDesync ) ) + centerX;
    int rightDesyncMaxY = centerY - ( radius * sin( DEG2RAD( 90 - maxDesync ) ) );
    int realX = radius * cos( DEG2RAD( 90 - realDiff ) ) + centerX;
    int realY = centerY - ( radius * sin( DEG2RAD( 90 - realDiff ) ) );
    int lbyX = radius * cos( DEG2RAD( 90 - lbyDiff ) ) + centerX;
    int lbyY = centerY - ( radius * sin( DEG2RAD( 90 - lbyDiff ) ) );

    static Color basicColor = Color( 0, 225, 225, 255 );
    static Color fakeColor = Color( 5, 200, 5, 255 ); // cyan
    static Color realColor = Color( 225, 5, 5, 255 ); //red
    static Color lbyColor = Color( 135, 235, 169, 255 );

    //Draw::AddCircle( centerX, centerY, radius, basicColor, 32 );
    Draw::OutlinedCircle( centerX, centerY, 32, radius, basicColor);

    Draw::Line( centerX, centerY, centerX, northY, fakeColor ); // Const North line
    Draw::Line( centerX, centerY, leftDesyncMaxX, leftDesyncMaxY, basicColor ); // Left Max
    Draw::Line( centerX, centerY, rightDesyncMaxX, rightDesyncMaxY, basicColor ); // Right Max

    if( Settings_AntiAim_Yaw_enabled && ( Settings_AntiAim_Yaw_typeFake != Settings_AntiAim_Yaw_type)  ){
        Draw::Line( centerX, centerY, realX, realY, realColor ); // Real Line
    }
    if( Settings_AntiAim_LBYBreaker_enabled ){
        Draw::Line( centerX, centerY, lbyX, lbyY, lbyColor ); // LBY Line
    }
}
