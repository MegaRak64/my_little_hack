#pragma once

#include "esp.h"
#include "../SDK/ICvar.h"
#include "../Utils/util.h"

#include <iostream>
#include <fstream>

bool esp_enabled = true;
bool noborder_enabled = false;
bool soundesp_enabled = false;
bool radarhack_enabled = true;

struct Footstep
{
	long expiration;
	int entityId;
	Vector position;
};

std::vector<Footstep> footsteps;

static bool GetBox( C_BaseEntity* entity, int& x, int& y, int& w, int& h ) {
	// Variables
	Vector vOrigin, min, max;
	Vector flb, brt, blb, frt, frb, brb, blt, flt; // think of these as Front-Left-Bottom/Front-Left-Top... Etc.
	float left, top, right, bottom;

	// Get the locations
	vOrigin = entity->GetVecOrigin();
	min = entity->GetCollideable()->OBBMins() + vOrigin;
	max = entity->GetCollideable()->OBBMaxs() + vOrigin;

	// Points of a 3d bounding box
	Vector points[] = { Vector( min.x, min.y, min.z ),
						Vector( min.x, max.y, min.z ),
						Vector( max.x, max.y, min.z ),
						Vector( max.x, min.y, min.z ),
						Vector( max.x, max.y, max.z ),
						Vector( min.x, max.y, max.z ),
						Vector( min.x, min.y, max.z ),
						Vector( max.x, min.y, max.z ) };

	// Get screen positions
	if ( debugOverlay->ScreenPosition( points[3], flb ) || debugOverlay->ScreenPosition( points[5], brt )
		 || debugOverlay->ScreenPosition( points[0], blb ) || debugOverlay->ScreenPosition( points[4], frt )
		 || debugOverlay->ScreenPosition( points[2], frb ) || debugOverlay->ScreenPosition( points[1], brb )
		 || debugOverlay->ScreenPosition( points[6], blt ) || debugOverlay->ScreenPosition( points[7], flt ) )
		return false;

	// Put them in an array (maybe start them off in one later for speed?)
	Vector arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };

	// Init this shit
	left = flb.x;
	top = flb.y;
	right = flb.x;
	bottom = flb.y;

	// Find the bounding corners for our box
	for ( int i = 1; i < 8; i++ ) {
		if ( left > arr[i].x )
			left = arr[i].x;
		if ( bottom < arr[i].y )
			bottom = arr[i].y;
		if ( right < arr[i].x )
			right = arr[i].x;
		if ( top > arr[i].y )
			top = arr[i].y;
	}

	// Width / height
	x = ( int ) left;
	y = ( int ) top;
	w = ( int ) ( right - left );
	h = ( int ) ( bottom - top );

	return true;
}

static void DrawBox(Color color, int x, int y, int w, int h, C_BaseEntity* entity)
{
Vector vOrigin = entity->GetVecOrigin();
		Vector min = entity->GetCollideable()->OBBMins() + vOrigin;
		Vector max = entity->GetCollideable()->OBBMaxs() + vOrigin;

		Vector points[] = { Vector( min.x, min.y, min.z ),
							Vector( min.x, max.y, min.z ),
							Vector( max.x, max.y, min.z ),
							Vector( max.x, min.y, min.z ),
							Vector( min.x, min.y, max.z ),
							Vector( min.x, max.y, max.z ),
							Vector( max.x, max.y, max.z ),
							Vector( max.x, min.y, max.z ) };

		int edges[12][2] = {
				{ 0, 1 },
				{ 1, 2 },
				{ 2, 3 },
				{ 3, 0 },
				{ 4, 5 },
				{ 5, 6 },
				{ 6, 7 },
				{ 7, 4 },
				{ 0, 4 },
				{ 1, 5 },
				{ 2, 6 },
				{ 3, 7 },
		};

		for ( const auto edge : edges ) {
			Vector p1, p2;
			if ( debugOverlay->ScreenPosition( points[edge[0]], p1 ) || debugOverlay->ScreenPosition( points[edge[1]], p2 ) )
				return;
			Draw::Line( p1.x, p1.y, p2.x, p2.y, color );
		}
}

static void DrawEntity( C_BaseEntity* entity, const char* string, Color color ) {
	int x, y, w, h;
	if ( !GetBox( entity, x, y, w, h ) )
		return;

	DrawBox( color, x, y, w, h, entity );
	Vector2D nameSize = Draw::GetTextSize( string, esp_font );
	Draw::Text(static_cast<int>((x + ( w / 2 ) - ( nameSize.x / 2 ))), y + h + 2, string, esp_font, color);
}

static void DrawDroppedWeapons(C_BaseCombatWeapon* weapon)
{
	Vector vOrig = weapon->GetVecOrigin();
	int owner = weapon->GetOwner();

	if (owner > -1 || (vOrig.x == 0 && vOrig.y == 0 && vOrig.z == 0))
		return;

	std::string modelName = Util::GetWeaponName(*weapon->GetItemDefinitionIndex());

	if (weapon->GetAmmo() > 0)
	{
		modelName += XORSTR(" | ");
		modelName += std::to_string(weapon->GetAmmo());
	}

	DrawEntity(weapon, modelName.c_str(), Color(225, 225, 0, 255));
}

static void DrawSkeleton( C_BasePlayer* player ) {
	studiohdr_t* pStudioModel = modelInfo->GetStudioModel( player->GetModel() );
	if ( !pStudioModel )
		return;

	static matrix3x4_t pBoneToWorldOut[128];
	if ( !player->SetupBones( pBoneToWorldOut, 128, 256, 0 ) )
		return;

	for ( int i = 0; i < pStudioModel->numbones; i++ ) {
		mstudiobone_t* pBone = pStudioModel->pBone( i );
		if ( !pBone || !( pBone->flags & 256 ) || pBone->parent == -1 )
			continue;

		Vector vBonePos1;
		if ( debugOverlay->ScreenPosition( Vector( pBoneToWorldOut[i][0][3], pBoneToWorldOut[i][1][3], pBoneToWorldOut[i][2][3] ), vBonePos1 ) )
			continue;

		Vector vBonePos2;
		if ( debugOverlay->ScreenPosition( Vector( pBoneToWorldOut[pBone->parent][0][3], pBoneToWorldOut[pBone->parent][1][3], pBoneToWorldOut[pBone->parent][2][3] ), vBonePos2 ) )
			continue;

		Draw::Line(vBonePos1.x, vBonePos1.y, vBonePos2.x, vBonePos2.y, Color(255, 0, 0, 255));
	}
}

static void DrawSounds()
{
	for (unsigned int i = 0; i < footsteps.size(); i++)
	{
		long diff = footsteps[i].expiration - Util::GetEpochTime();
		if (diff <= 0)
		{
			footsteps.erase(footsteps.begin() + i);
			continue;
		}

		Vector pos2d;

		if (debugOverlay->ScreenPosition(footsteps[i].position, pos2d))
			continue;

		C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
		if (!localplayer)
			continue;

		C_BasePlayer* player = (C_BasePlayer*) entityList->GetClientEntity(footsteps[i].entityId);
		if (!player)
			continue;

		if (player->GetTeam() == localplayer->GetTeam())
			continue;

		//bool bIsVisible = false;
		//if (Settings::ESP::Filters::visibilityCheck || Settings::ESP::Filters::legit)
		//	bIsVisible = Entity::IsVisible(player, (int)Bone::BONE_HEAD, 180.f, Settings::ESP::Filters::smokeCheck);

		float percent = (float)diff / 2000.f;

		//Color playerColor = Color::FromImColor(ESP::GetESPPlayerColor(player, bIsVisible));
		//playerColor.a = std::min(powf(percent * 2, 0.6f), 1.f) * playerColor.a; // fades out alpha when its below 0.5
		auto playerColor = Color(255, 0, 0, 255);
		//playerColor.a = std::min(powf(percent * 2, 0.6f), 1.f) * playerColor.a; // fades out alpha when its below 0.5

		float circleRadius = fabs(percent - 1.f) * 42.f;
		float points = circleRadius * 0.75f;

		Draw::Circle3D(footsteps[i].position, points, circleRadius, playerColor);
		
	}
}

static void DrawPlayer(C_BasePlayer* player, C_BasePlayer* localplayer)
{
	if (!player || player == localplayer || !player->GetAlive() || player->GetTeam() == localplayer->GetTeam())
		return;
	if (radarhack_enabled)
		*player->GetSpotted() = true;
	int x, y, w, h;
	if (!GetBox(player, x, y, w, h))
		return;
	//DrawBox(Color(255, 0, 0, 255), x, y, w, h, player);
	DrawSkeleton(player);
	std::string displayString;
		
	IEngineClient::player_info_t playerInfo;
	engine->GetPlayerInfo( player->GetIndex(), &playerInfo );
	displayString += playerInfo.name;
	displayString += " ";
	displayString += std::to_string(player->GetHealth()) + XORSTR(" HP");
	
	Vector2D nameSize = Draw::GetTextSize(displayString.c_str(), esp_font);
	static Vector2D textSize = Draw::GetTextSize(XORSTR("Hi"), esp_font);
	Draw::Text((int)(x + (w / 2) - (nameSize.x / 2)), (int)(y - textSize.y), (const char*) displayString.c_str(), esp_font, Color(255, 255, 0, 255));
}

bool ESP::PrePaintTraverse(VPANEL vgui_panel, bool force_repaint, bool allow_force)
{
	if (esp_enabled && noborder_enabled && strcmp(XORSTR("HudZoom"), panel->GetName(vgui_panel)) == 0)
		return false;

	return true;
}

void ESP::Paint()
{
	if (!esp_enabled)
		return;
		
	if (!engine->IsInGame())
		return;

	//C_BasePlayer* localplayer = (C_BasePlayer*) entityList->GetClientEntity(engine->GetLocalPlayer());
	C_BasePlayer* localplayer = static_cast<C_BasePlayer*>(entityList->GetClientEntity(engine->GetLocalPlayer()));
	if (!localplayer)
		return;
		
	for (int i = 1; i < entityList->GetHighestEntityIndex(); ++i)
	{
		C_BaseEntity* entity = entityList->GetClientEntity(i);
		if (!entity)
			continue;
		
		ClientClass* client = entity->GetClientClass();
		
		switch (client->m_ClassID) {
			case EClassIds::CCSPlayer:
				DrawPlayer(static_cast<C_BasePlayer*>(entity), localplayer);
				continue;
				break;
			case EClassIds::CDEagle: case EClassIds::CAK47: case EClassIds::CWeaponAug ... EClassIds::CWeaponXM1014:
			case EClassIds::CFlashbang: case EClassIds::CDecoyGrenade: case EClassIds::CHEGrenade: case EClassIds::CSmokeGrenade:
				DrawDroppedWeapons(static_cast<C_BaseCombatWeapon*>(entity));
				break;
			default:
				break;
		}
	}
	DrawSounds();
}

void ESP::FireGameEvent(IGameEvent* event)
{
	if (!soundesp_enabled)
		return;

	if (!engine->IsInGame())
		return;

	if (strcmp(event->GetName(), XORSTR("player_footstep")) != 0) // event -> sound
		return;
	
	int player_id = event->GetInt(XORSTR("userid"));
	
	if (!entityList->GetClientEntity(player_id))
		return;
	
	if (engine->GetPlayerForUserID(player_id) == engine->GetLocalPlayer() || entityList->GetClientEntity(player_id)->GetTeam() == entityList->GetClientEntity(engine->GetLocalPlayer())->GetTeam())
		return;
	
	Footstep footstep;
	footstep.entityId = player_id;
	footstep.position = entityList->GetClientEntity(player_id)->GetVecOrigin();
	footstep.expiration = Util::GetEpochTime() + 2000;

	footsteps.push_back(footstep);
}
