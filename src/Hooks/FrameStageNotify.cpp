#include "hooks.h"

void Hooks::FrameStageNotify(void* thisptr, ClientFrameStage_t stage)
{
	CustomGlow::FrameStageNotify(stage);
	SkinChanger::FrameStageNotifyModels(stage);
	Resolver::FrameStageNotify(stage);
	
	if (SkinChanger::forceFullUpdate)
	{
		GetLocalClient(-1)->m_nDeltaTick = -1;
		SkinChanger::forceFullUpdate = false;
	}
	clientVMT->GetOriginalMethod<FrameStageNotifyFn>(37)(thisptr, stage);
	
	Resolver::PostFrameStageNotify(stage);
}
