#include "hooks.h"

void Hooks::SetMouseCodeState(void* thisptr, ButtonCode_t code, MouseCodeState_t state)
{
	inputInternalVMT->GetOriginalMethod<SetMouseCodeStateFn>(93)(thisptr, code, state);
}
