#include "hooks.h"

float Hooks::GetViewModelFOV(void* thisptr)
{
	float fov = clientModeVMT->GetOriginalMethod<GetViewModelFOVFn>(36)(thisptr);

	return fov;
}
