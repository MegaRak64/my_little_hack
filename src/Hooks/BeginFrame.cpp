#include "hooks.h"

void Hooks::BeginFrame(void* thisptr, float frameTime)
{
	DisablePostProcessing::BeginFrame();
	if (!engine->IsInGame())
		CreateMove::sendPacket = true;

	return materialVMT->GetOriginalMethod<BeginFrameFn>(42)(thisptr, frameTime);
}
