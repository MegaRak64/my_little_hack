#include "fonts.h"
#include <iterator>
#include <vector>
#include <fstream>
#include <algorithm>

HFont esp_font = 0;
HFont menu_font = 0;

void Fonts::SetupFonts()
{
	esp_font = Draw::CreateFont((char*) "Segoe UI", 11, 0x108);
							
	menu_font = Draw::CreateFont((char*) "Comic Sans", 14, 0x108);
}
