#pragma once
#include <thread>
#include <chrono>
#include <fcntl.h>
#include <sys/stat.h>

#include "Hooks/hooks.h"
#include "hooker.h"
#include "interfaces.h"
#include "Utils/util.h"

namespace SUKA_BLYAT
{
	void SelfShutdown();
	extern char buildID[NAME_MAX];
}
