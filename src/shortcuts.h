#pragma once

#include "SDK/SDK.h"
#include "Hacks/hacks.h"

namespace Shortcuts
{
	void SetKeyCodeState(ButtonCode_t code, bool bPressed);
}
