#include "shortcuts.h"

void Shortcuts::SetKeyCodeState(ButtonCode_t code, bool bPressed)
{
	if (!bPressed)
		return;
	switch (code) {
		case ButtonCode_t::KEY_PAD_0:
			submenu_blyat = 0;
			if (submenu_status)
				submenu_status = 0;
			else
				submenu_status = 10;
			key_code_status = 0;
			submenu_call = false; // Maybe need move to upper
			page = 0;
			break;
		case ButtonCode_t::KEY_PAD_1:
			if (!submenu_status)
				submenu_status = 1;
			key_code_status = 1;
			break;
		case ButtonCode_t::KEY_PAD_2:
			if (!submenu_status)
				submenu_status = 2;
			key_code_status = 2;
			break;
		case ButtonCode_t::KEY_PAD_3:
			if (!submenu_status)
				submenu_status = 3;
			key_code_status = 3;
			break;
		case ButtonCode_t::KEY_PAD_4:
			if (!submenu_status)
				submenu_status = 4;
			key_code_status = 4;
			break;
		case ButtonCode_t::KEY_PAD_5:
			if (!submenu_status)
				submenu_status = 5;
			key_code_status = 5;
			break;
		case ButtonCode_t::KEY_PAD_6:
			if (!submenu_status)
				submenu_status = 6;
			key_code_status = 6;
			break;
		case ButtonCode_t::KEY_PAD_7:
			if (!submenu_status)
				submenu_status = 7;
			key_code_status = 7;
			break;
		case ButtonCode_t::KEY_PAD_8:
			if (!submenu_status)
				submenu_status = 8;
			key_code_status = 8;
			break;
		case ButtonCode_t::KEY_PAD_9:
			if (!submenu_status)
				submenu_status = 9;
			key_code_status = 9;
			break;
		case ButtonCode_t::KEY_PAD_PLUS:
			key_code_status = 10;
			break;
		case ButtonCode_t::KEY_PAD_MINUS:
			key_code_status = 11;
			break;
		default:
			break;
		}
}
